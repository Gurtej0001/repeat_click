import 'dart:async';

import 'package:flutter/material.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/LoginScreen.dart';
import 'package:repeat_click/utils/MyCustomClipper.dart';
import 'package:repeat_click/utils/draw_arc_bottom.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Colors/Colors.dart';
import '../Model/introModel.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {

  List<IntroModel> mySLides = new List<IntroModel>();
  PageController controller;
  int slideIndex = 0;

  var _paint = Paint()
    ..color = Colors.white
    ..strokeWidth = 10.0
    ..style = PaintingStyle.fill;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    mySLides = getSlides();
    controller = new PageController();
    Timer.periodic(Duration(seconds: 4), (Timer timer) {
      if (slideIndex < 2) {
        slideIndex++;
      } else {
        timer.cancel();
       // slideIndex = 0;
        openLoginScreen(context);

      }

      controller.animateToPage(
        slideIndex,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeIn,
      );
    });
    //getUserData();
    setState(() {

    });
  }

  static openLoginScreen(BuildContext context) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setBool(WebApiConstaint.kIsIntroScreen, false);
    await Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return LoginScreen();
      },
    ));
  }

  @override
  Widget build(BuildContext context) {
    var screenWidth =  MediaQuery.of(context).size.width;
    var screenHeight =  MediaQuery.of(context).size.height;
    return Scaffold(
      backgroundColor: orangeColor,
      body: Stack(
        children: [
          new Container(
            color: Colors.white,
            height: MediaQuery.of(context).size.height / 2 - 50, //400.0,
            width: MediaQuery.of(context).size.width,
            child: CustomPaint(
              painter: DrawArc(
                  _paint,
                  -150,
                  -220,
                  MediaQuery.of(context).size.width + 400,
                  MediaQuery.of(context).size.height / 2 + 100),
            ),
          ),
          PageView(
            controller: controller,
            onPageChanged: (index) {
              setState(() {
                slideIndex = index;
              });
            },
            children: <Widget>[
              SlideTile(
                imagePath: mySLides[0].getImageAssetPath(),
                title: mySLides[0].getTitle(),
                desc: mySLides[0].getDesc(),
                size: mySLides.length,
                index: 0,
              ),
              SlideTile(
                imagePath: mySLides[1].getImageAssetPath(),
                title: mySLides[1].getTitle(),
                desc: mySLides[1].getDesc(),
                size: mySLides.length,
                index: 1,
              ),
              SlideTile(
                imagePath: mySLides[2].getImageAssetPath(),
                title: mySLides[2].getTitle(),
                desc: mySLides[2].getDesc(),
                size: mySLides.length,
                index: 2,
              )
            ],
          ),
        ],

      ),
    );
  }
}
_buildPageIndicator(bool isCurrentPage) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 5.0),
    height: isCurrentPage ? 18.0 : 18.0,
    width: isCurrentPage ? 18.0 : 18.0,
    decoration: BoxDecoration(
      color: isCurrentPage ? Colors.white : Colors.transparent,
      borderRadius: BorderRadius.circular(9),
        border: Border.all(color: Colors.white)
    ),
  );
}
class SlideTile extends StatelessWidget {
  String imagePath, title, desc;
  int index = 0, size = 0;

  SlideTile({this.imagePath, this.title, this.desc, this.size, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FittedBox(
            child: Image.asset(imagePath),
            fit: BoxFit.fill,
          ),

          SizedBox(
            height: 150,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 24.0,
              color: Colors.white,
              fontFamily: 'PoppinsSemiBold',),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            desc,
            textAlign: TextAlign.center,
            style: TextStyle(fontSize: 15.0,
              color: Colors.white,
              fontFamily: 'Poppins',),
          ),

          SizedBox(
            height: 60,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                for (int i = 0; i < size; i++)
                  i == index
                      ? _buildPageIndicator(true)
                      : _buildPageIndicator(false),
              ],
            ),
          ),
        ],
      ),
    );
  }


}
