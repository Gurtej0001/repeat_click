import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/HomeBaseScreen.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:repeat_click/API/web_API.dart';
import 'SecurePinScreen.dart';


class SetupPinScreen extends StatefulWidget {
  final bool isChangePassword;

  SetupPinScreen({Key key, @required this.isChangePassword}) : super(key: key);
  @override
  _SetupPinScreenState createState() => _SetupPinScreenState();
}

class _SetupPinScreenState extends State<SetupPinScreen> {
  TextEditingController txtOldPin = TextEditingController();
  TextEditingController txtSetPin = TextEditingController();
  TextEditingController txtConfirmPin = TextEditingController();
  FocusNode myFocusNode;
  String authKey;
  // bool isCorrectPin;

  @override
  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {

    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
        backgroundColor: whiteColor,
        body:GestureDetector (
          onTap: (){
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Column(
              children: [
                new Container(
                  height: MediaQuery.of(context).size.height / 2 - 50,
                  width: MediaQuery.of(context).size.width,
                  color: Colors.white,
                  child: Image.asset('assets/image/downCurve.png',fit: BoxFit.fill,),
                ),
                new Padding(
                  padding: const EdgeInsets.all(20),
                  child: new Text(
                   widget.isChangePassword ?'Change Pin' : setupPinText,
                    style: titleTextStyle,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top:20,left: 20,right: 20),
                  child: Column(
                    children: [
                     // Container(

                      //  child :
                      // Container(
                        //child :
                        widget.isChangePassword ? SizedBox(
                            height: 70.0,
                            child: TextFormField(
                              controller: txtOldPin,
                              maxLength: 6,
                              keyboardType: TextInputType.number,
                              style: textFieldTextStyle,
                              textInputAction: TextInputAction.next,
                              decoration: InputDecoration(
                                // icon: Icon(CupertinoIcons.down_arrow),
                                labelText: "Old M-Pin",

                                border: OutlineInputBorder(
                                  borderRadius:
                                  BorderRadius.all(Radius.circular(30.0)),
                                ),

                              ),
                            )) : SizedBox.shrink(


                        ),

                      // ),
                      SizedBox(
                        height: 10,
                      ),

                        SizedBox(
                          height: 70.0,
                          child : TextFormField(
                            controller: txtSetPin,
                            textInputAction: TextInputAction.next,
                            style: textFieldTextStyle,
                            maxLength: 6,
                            decoration: InputDecoration(
                              // icon: Icon(CupertinoIcons.down_arrow),
                              labelText: widget.isChangePassword ? 'New M-Pin':"Enter M-Pin",
                              // labelStyle:
                              // Light14Style.copyWith(color: hintTextColor
                              // ),
                              //errorText:
                              border: OutlineInputBorder(
                            borderRadius:
                            BorderRadius.all(Radius.circular(30.0)),
                        ),
                              //contentPadding: EdgeInsets.all(10),

                            ),
                            keyboardType: TextInputType.number,

                          ),
                        ),


                      //),
                      SizedBox(
                        height: 10,
                      ),

                        SizedBox(
                          height: 70.0,
                          child: new TextFormField(

                            controller: txtConfirmPin,

                            textInputAction: TextInputAction.done,
                            style: textFieldTextStyle,
                            maxLength: 6,
                            decoration: InputDecoration(

                              labelText: "Confirm M-Pin",
                              // labelStyle:
                              // Light14Style.copyWith(color: hintTextColor
                              // ),
                              //errorText:
                              border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(30.0)),
                              ),
                              //contentPadding: EdgeInsets.all(10),

                            ),
                            keyboardType: TextInputType.number,

                            // ),

                          ),
                        ),

                      SizedBox(
                        height: 25,
                      ),

                      SizedBox(

                        width: double.infinity,
                        child: FlatButton(
                          height: 50,
                          onPressed: ()async{

                            if (widget.isChangePassword && txtOldPin.text == ""){
                              Fluttertoast.showToast(msg: 'Please Enter Old M-Pin');
                            }else if (txtSetPin.text == ""){
                              Fluttertoast.showToast(msg: 'Please Enter M-Pin');
                            }else if (txtSetPin.text.length <6){
                              Fluttertoast.showToast(msg: 'Please Enter M-Pin with 6 digit length');
                            }else if (txtConfirmPin.text == "" || txtConfirmPin.text.length <6 || txtSetPin.text != txtConfirmPin.text){
                              Fluttertoast.showToast(msg: 'M-Pin does not match');
                            }else{
                              final prefs = await SharedPreferences.getInstance();
                              var strPin = prefs.get(WebApiConstaint.kQuickPin);
                              if (widget.isChangePassword){
                                if (strPin != txtOldPin.text){
                                  Fluttertoast.showToast(msg: 'Incorrect old M-Pin');
                                }else {

                                  setUpPin();
                                }

                              }else{

                                  setUpPin();

                              }

                                //openSecurePinScree();
                            }

                          },
                          child: Text(
                            continueText.toUpperCase(),
                            style: buttonTextStyle,
                          ),
                          color: orangeColor,
                          shape: shape,
                        ),
                      ),

                      SizedBox(
                        height: 50,
                      ),

                      !widget.isChangePassword ? SizedBox(

                          width: double.infinity,
                          child: FlatButton(
                            height: 50,
                            onPressed: (){
                              openHomeScreen();
                            },
                            child: Text(
                              skipNowText.toUpperCase(),
                              style: buttonTextStyle,
                            ),
                            color: darkBlue,
                            shape: shape,
                          )) :SizedBox.shrink(

                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),

                ),

              ],
            ),
          ),
        ),

        );
  }
  _setQuickPin() async {
   // SharedPreferences prefs = await SharedPreferences.getInstance();
    SharedPreferences prefs = await SharedPreferences.getInstance();
    // if (prefs.containsKey(WebApiConstaint.kQuickPin)){
    //       prefs.remove(WebApiConstaint.kQuickPin);
    //     }
    prefs.setString(WebApiConstaint.kQuickPin, txtConfirmPin.text);
    Fluttertoast.showToast(
              msg: "Pin set successfully");
    openSecurePinScreen();
  }

  openHomeScreen()async{
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => HomeBaseScreen()),
    );
  }
  void openSecurePinScreen(){
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SecurePinScreen()),
    );
  }
  void setUpPin() async {

    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(WebApiConstaint.kQuickPin)){
      prefs.remove(WebApiConstaint.kQuickPin);
    }
    Fluttertoast.showToast(
        msg: "Pin set successfully");
    prefs.setString(WebApiConstaint.kQuickPin, txtConfirmPin.text);
    openSecurePinScreen();
  }
}

