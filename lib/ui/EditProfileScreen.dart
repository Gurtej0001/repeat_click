import 'dart:convert';
import  'dart:io';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/LoginScreen.dart';
import 'package:repeat_click/ui/SetupPinScreen.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class EditProfileScreen extends StatefulWidget {
  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {

  Dio dio = new Dio();
  ApiProvider provider = ApiProvider();
  Map<dynamic, dynamic> dictProfile;
  TextEditingController txtName = TextEditingController();
  TextEditingController txtEmail = TextEditingController();
  TextEditingController txtPhone = TextEditingController();
  FocusNode myFocusNode;
  bool _validateName = false;
  bool _validateEmail = false;
  bool _validatePhone = false;
  File _image;
  String authKey = "";
  String sessionKey = "";
  final picker = ImagePicker();

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();

  }

  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey(WebApiConstaint.kUserData);

    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    print(authKey);
    if (checkValue){
      dictProfile = json.decode(prefs.getString(WebApiConstaint.kUserData));
      print(dictProfile);
      txtName.text = dictProfile['name'];
      txtEmail.text = dictProfile['email'];
      txtPhone.text = dictProfile['mobile_number'];
      if (dictProfile['image'] != null && dictProfile['image'] != ''){
        var rng = new Random();
        Directory tempDir = await getTemporaryDirectory();
        String tempPath = tempDir.path;
        _image = new File('$tempPath'+ (rng.nextInt(100)).toString() +'.png');
        http.Response response = await http.get(dictProfile['image']);
        await _image.writeAsBytes(response.bodyBytes);
        print(_image);

      }
    }
  }

  @override
  Widget build(BuildContext context) {
    getUserData();
    return Scaffold(
        body: GestureDetector(
          onTap: (){
            FocusScope.of(context).requestFocus(new FocusNode());
          },
          child: SingleChildScrollView(
            child: Column(
            children: [
            //  dictProfile != null ?
              Container(
                height: 240,

                child: Stack(
                  children: <Widget>[
                    Container(
                      color: orangeColor,
                      width: MediaQuery.of(context).size.width,
                      height: 160.0,
                      child: Column(
                        children: [
                          Padding(
                            padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                            child: Text(
                              dictProfile['salutation'] + " " +dictProfile['name'],
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 16.0,
                                color: whiteColor,
                                fontFamily: 'PoppinsRegular',
                              ),
                            ),
                          ),
                          Text(
                            dictProfile['designation'],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: whiteColor,
                              fontFamily: 'PoppinsRegular',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 85,
                      left: 100,
                      right: 100,
                      child: InkWell(
                        onTap: (){
                            _showPicker(context);
                        },
                        child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey[800], spreadRadius: 1.5)],
                          ),
                          child: CircleAvatar(
                            radius: 50,
                            backgroundColor: Color(0xffFDCF09),

                            child: dictProfile != null && dictProfile['image'] != null && dictProfile['image'] != '' ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  dictProfile['image'],
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                )
                            ):_image != null
                                ? ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.file(
                                  _image,
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                )
                            ) : Container(
                              decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(50)),
                              width: 100,
                              height: 100,
                              child: Icon(
                                Icons.camera_alt,
                                color: Colors.grey[800],
                              ),
                            ),
                          ) ,
                        ),
                      ),

                    ),


                  ],
                ),
              ) ,
              //     : SizedBox(
              //   height: 5,
              // ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Icon(Icons.logout, color: orangeColor,),
                        InkWell(
                          onTap: (){
                            _onWillPop();
                          },
                          child:  new Text(
                            "Logout",
                            style: TextStyle(
                              fontSize: 16.0,
                              color: orangeColor,
                              fontFamily: 'PoppinsSemiBold',
                            ),
                          ),
                        )

                      ],
                    ),

                    Container(
                      height:90.0,
                      padding: const EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: new TextFormField(
                        controller: txtName,
                        textInputAction: TextInputAction.next,
                        style: textFieldTextStyle,
                        maxLength: 10,
                        maxLines: 1,
                        decoration: new InputDecoration(
                          counterText: "",
                          border: border,
                          focusedBorder: focusedBorder,
                          hintText: nameText,
                          hintStyle: hintStyle,
                          contentPadding: EdgeInsets.all(15),
                          errorText: _validateName ? 'Please enter your Name' :txtName.text == ''? null :  WebApiConstaint.nameRegExp.hasMatch(txtName.text) ? null : "Please enter valid name",
                        ),
                          keyboardType: TextInputType.name,

                      ),
                    ),

                    Container(
                      height : 90.0,
                      padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: new TextField(
                        controller: txtEmail,
                        textInputAction: TextInputAction.next,
                        style: textFieldTextStyle,
                        // maxLength: 10,
                        maxLines: 1,
                        decoration: new InputDecoration(
                          counterText: "",
                          border: border,
                          focusedBorder: focusedBorder,
                          hintText: emailText,
                          hintStyle: hintStyle,
                          contentPadding: EdgeInsets.all(15),
                          errorText: _validateEmail ? 'Please enter your Email' :txtEmail.text == "" ? null :WebApiConstaint.emailRexExp.hasMatch(txtEmail.text)? null : "Please enter valid email",
                        ),
                          keyboardType: TextInputType.emailAddress
                      ),
                    ),
                    Container(
                      height : 90.0,
                      padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
                      child: new TextField(
                        controller: txtPhone,
                        textInputAction: TextInputAction.done,
                        style: textFieldTextStyle,
                        maxLength: 10,
                        maxLines: 1,
                        decoration: new InputDecoration(
                          counterText: "",
                          border: border,
                          focusedBorder: focusedBorder,
                          hintText: phoneText,
                          hintStyle: hintStyle,
                          contentPadding: EdgeInsets.all(15),
                          errorText: _validatePhone ? 'Please enter your Phone No' : null,
                        ),
                          keyboardType: TextInputType.number
                      ),
                    ),
                    Container(
                      height:  50.0,
                      padding: const EdgeInsets.only(top: 0),
                      child: new SizedBox(
                          width: double.infinity,
                          // height: double.infinity,
                          child: FlatButton(
                            height: 50,
                            child: Text(
                              updateText.toUpperCase(),
                              style: buttonTextStyle,
                            ),
                            color: orangeColor,
                            shape: shape,
                            onPressed: () {
                              setState(() {
                                txtName.text.isEmpty ?  _validateName = true : _validateName = false;
                                txtEmail.text.isEmpty ?  _validateEmail= true : _validateEmail = false;
                                txtPhone.text.isEmpty ?  _validatePhone = true : _validatePhone = false;

                              });
                              if (_image == null){

                                Fluttertoast.showToast(msg: 'Please upload profile photo');
                              }else{
                                if(!_validateName&&!_validateEmail&&!_validatePhone&&WebApiConstaint.nameRegExp.hasMatch(txtName.text)&&WebApiConstaint.emailRexExp.hasMatch(txtEmail.text)){
                                  print('Pratibha agrawal');
                                  uploadImage();


                                }
                              }



                            },
                          )),
                    ),

                    InkWell(
                        onTap: (){
                          openChangePasswordScreen();
                        },
                        child : Padding(
                            padding: EdgeInsets.only(top: 15,right: 20),
                            child :Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                    changePasswordText,
                                    style: buttonTextStyle.copyWith(color: orangeColor)
                                ),
                              ],
                            )
                        )



                    )
                  ],
                ),
              ),
            ],
          ),
        ),));
  }
  openChangePasswordScreen(){
    Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return SetupPinScreen(isChangePassword: true);
      },
    ));
  }


  logOut()async{

    final prefs = await SharedPreferences.getInstance();
    prefs.remove(WebApiConstaint.kQuickPin);
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginScreen()),
    );
  }
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Logout'),
        content: new Text('Are you sure you want to logout?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => {
              logOut(),
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    )) ??
        false;
  }
  Future imgFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        if (dictProfile != null && dictProfile['image'] != ""){
          dictProfile['image'] = "";
        }
        _image = File(pickedFile.path);

      } else {
        print('No image selected.');
      }

    });

  }

  Future imgFromGallery() async {
    final pickedFile = await  picker.getImage(
        source: ImageSource.gallery, imageQuality: 50
    );
    // if (pickedFile != null) {
    //   if (dictProfile != null && dictProfile['profilePhoto'] != ""){
    //     dictProfile['profilePhoto'] = "";
    //   }
    //   _image = File(pickedFile.path);
    // } else {
    //   print('No image selected.');
    // }
    if (pickedFile != null){
      if (dictProfile != null && dictProfile['image'] != ""){
            dictProfile['image'] = "";
          }
      _image = File(pickedFile.path);
    }

    this.setState(() {

    });
  }
  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
  void uploadImage() async {

    FormData formData = new FormData.fromMap({
      "name": txtName.text,
      "email": txtEmail.text,
      "image" : await MultipartFile.fromFile(_image.path, filename:'file'),
    });
    print(formData);
    print(authKey);
    print(sessionKey);
    EasyLoading.show();
    provider.uploadImage(formData,authKey,sessionKey,WebApiConstaint.API_URL_UPDATE_PROFILE)..then((responce) async{
      if(responce.data["error"] == false){
        EasyLoading.dismiss();
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        dictProfile = responce.data["data"];
        final prefs = await SharedPreferences.getInstance();
        prefs.remove(WebApiConstaint.kUserData);
        prefs.setString(WebApiConstaint.kUserData, json.encode(dictProfile));

        setState(() {

        });

      }else{
        EasyLoading.dismiss();
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);

      }
      EasyLoading.dismiss();
    });


  }
}


