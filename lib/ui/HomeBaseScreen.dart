import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/EditProfileScreen.dart';
import 'package:repeat_click/ui/LoginScreen.dart';
import 'package:repeat_click/ui/home/ShiftScreen.dart';
import 'package:repeat_click/ui/myAttendance.dart';
import 'package:repeat_click/ui/mySalary.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SliderScreen.dart';
import 'home/HomeScreen.dart';
import 'package:repeat_click/ui/notification.dart';
import 'package:intl/intl.dart';

class HomeBaseScreen extends StatefulWidget {
  @override
  HomeBaseScreenState createState() => HomeBaseScreenState();
}

class HomeBaseScreenState extends State<HomeBaseScreen> {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final selectedItemColor = whiteColor;
  final unselectedItemColor = whiteColor;
  final selectedBgColor = darkBlue;
  final unselectedBgColor = orangeColor;
  ApiProvider provider = ApiProvider();
  Map<dynamic, dynamic> dictSalaryParam;
  Map<dynamic, dynamic> dictAttendanceParam;
  Map<dynamic, dynamic> dictShiftParam;
  String strName = "";
  String strTitle = "Hi";
  int selectedIndex = 0;
  String strDate = 'Select Year & Month';

  String authKey;
  String sessionKey;
  int status;
  bool isError;


  Color _getBgColor(int index) =>
      selectedIndex == index ? selectedBgColor : unselectedBgColor;

  Color _getItemColor(int index) =>
      selectedIndex == index ? selectedItemColor : unselectedItemColor;

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
      switch(index) {
        case 0: { strTitle = 'Hi '+strName;getShift(); }
        break;
        case 1: { strTitle = 'My shifts'; getShift();}
        break;
        case 2: {
          strTitle = 'My salary';
          getSalary();
        }
        break;
        case 3: { strTitle = 'My attendance'; getAttendance();}
        break;
        case 4: { strTitle = 'My Profile'; }
        break;
      }
    });
  }

  Widget buildIcon(IconData iconData, String text, int index, bool isIcon) => Container(
        width: double.infinity,
        height: kBottomNavigationBarHeight,
        child: Material(
          color: _getBgColor(index),
          child: InkWell(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                isIcon ?
                Icon(iconData) : Image(image: AssetImage("assets/icons/pound.png"),height: 22,width: 20,),
                Text( index == 3 ? selectedIndex == 3 ? text : "Attend.." : text,
                    style: TextStyle(
                        fontSize: 14,
                        color: _getItemColor(index),
                        fontFamily: 'PoppinsRegular')),
              ],
            ),
            onTap: () => _onItemTapped(index),
          ),
        ),
      );

  @override
  void initState() {
    // TODO: implement initState
    super.initState();


      var currDt = DateTime.now();
      var strYear = currDt.year;
      final DateFormat formatter = DateFormat('MMMM-yyyy');
      strDate = formatter.format(currDt);

      getUserData();

  }
  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey(WebApiConstaint.kUserData);
    Map<dynamic,dynamic> dictParam;
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    if (checkValue){
      dictParam = json.decode(prefs.getString(WebApiConstaint.kUserData));
      strName = dictParam['name'];
      getShift();
      print(dictParam);
    }
    if (dictParam != null){
      strTitle = 'Hi ' + strName;
    }

  }

  @override
  Widget build(BuildContext context) {

    return new WillPopScope(
        onWillPop: _onWillPop,
      child : Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        elevation: 0.0,
        title: Text(
          strTitle,
          style: TextStyle(
            fontSize: 20.0,
            color: whiteColor,
            fontFamily: 'PoppinsRegular',
          ),
        ),
        actions: <Widget>[
          new IconButton(
            icon: new Icon(Icons.notification_important_outlined),
            onPressed: () {
              openNotificationScreen();

            },
          ),
        ],
      ),
      body: IndexedStack(
        index: selectedIndex,
        children: <Widget>[HomeScreen(btnStartEndState: status,), ShiftScreen(dictParam: dictShiftParam,isError:isError,), SalaryScreen(dictParam: dictSalaryParam,isError:isError ,), AttendanceScreen(dictParam: dictAttendanceParam, isSelect: true), EditProfileScreen()],
      ),
      // child: _widgetOptions.elementAt(selectedIndex),
      bottomNavigationBar: BottomNavigationBar(

        currentIndex: selectedIndex,
        selectedItemColor: selectedItemColor,
        unselectedItemColor: unselectedItemColor,
       // onTap: onTabTapped,
        selectedFontSize: 0,
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: buildIcon(Icons.home_outlined, homeText, 0, true),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: buildIcon(Icons.business_center_outlined, shiftText, 1,true),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: buildIcon(Icons.star, salaryText, 2,false),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: buildIcon(Icons.timer, attendanceText, 3,true),
            title: SizedBox.shrink(),
          ),
          BottomNavigationBarItem(
            icon: buildIcon(Icons.person_rounded, profileText, 4,true),
            title: SizedBox.shrink(),
          ),
        ],

      ),

    ));
  }
  void onTabTapped(int index) {
    setState(() {
      selectedIndex = index;
      switch(index) {
        case 0: { strTitle = 'Hi' + strName;getShift(); }
        break;
        case 1: { strTitle = 'My shifts'; getShift();}
        break;
        case 2: { strTitle = 'My salary';getSalary(); }
        break;
        case 3: { strTitle = 'My attendance';getAttendance(); }
        break;
        case 4: { strTitle = 'My Profile'; }
        break;
      }
    });
  }
  openNotificationScreen()async{
    await Navigator.push(context, MaterialPageRoute(
      builder: (context) {
        return NotificationScreen();
      },
    ));
  }
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => SystemNavigator.pop(),
            child: new Text('Yes'),
          ),
        ],
      ),
    )) ??
        false;
  }



  getSalary()async{

    EasyLoading.show();
    String strUrl = WebApiConstaint.API_URL_STAFF_SALARY + '?month='+ strDate;

    provider.requestGetForApi(strUrl, authKey,sessionKey ).then((responce)  async {

      if(responce.data["error"] == false) {
        dictSalaryParam = responce.data;
        this.setState(() {
          print(responce.data);
        });
      }else{

        isError = true;
        this.setState(() {
          print(responce.data);
        });
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);
      }
      EasyLoading.dismiss();
    });
  }

  getAttendance()  {
    EasyLoading.show();
    String strUrl = WebApiConstaint.API_URL_STAFF_ATTENDENCE + '?month='+ strDate;
    provider.requestGetForApi( strUrl, authKey ,sessionKey).then((responce)  {

      if(responce.data["error"] == false) {

        dictAttendanceParam = responce.data;
        this.setState(() {
          print(responce.data);
        });
      }else{
        Fluttertoast.showToast(
            msg: responce.data["message"]
        );
        print(responce.data["message"]);
      }
      EasyLoading.dismiss();
    });
  }

  getShift(){
    String strUrl = WebApiConstaint.API_URL_GET_SHIFT;
    EasyLoading.show();
    provider.requestGetForApi(strUrl, authKey,sessionKey ).then((responce)  async {

      if(responce.data["error"] == false) {
        dictShiftParam = null;
        dictShiftParam = responce.data;
        print(dictShiftParam['totalWorkHr'].toString()+':'+dictShiftParam['totalWorkMins'].toString()+"hr");
        status = dictShiftParam['status'];
          print(responce.data);
          setState(() {

          });
      }else{
        isError = true;
        setState(() {

        });
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);
      }
      EasyLoading.dismiss();
    });
  }

}
