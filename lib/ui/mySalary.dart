import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:shared_preferences/shared_preferences.dart';




class SalaryScreen extends StatefulWidget {
   Map<dynamic, dynamic> dictParam;
   bool isError = false;
  SalaryScreen({Key key, @required this.dictParam,this.isError}) : super(key: key);
  @override
  _SalaryScreenState createState() => _SalaryScreenState();
}

class _SalaryScreenState extends State<SalaryScreen> {

  ApiProvider provider = ApiProvider();
  int displayedYear;
  bool isDatePicker;
  String strDate = 'Select Year & Month';
  int year;


  @override
  void initState() {
    super.initState();
    isDatePicker = false;
    widget.isError = false;
    var currDt = DateTime.now();
    var strYear = currDt.year;
    year = strYear.toInt();
    final DateFormat formatter = DateFormat('MMMM-yyyy');
    strDate = formatter.format(currDt);
    // getSalary();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isError == null){
      widget.isError = false;
    }
    print(widget.dictParam);

    return Scaffold(
      body:widget.dictParam != null ? Column(
        mainAxisAlignment: MainAxisAlignment.start,
        //crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 60,right: 60,top: 30),
            child: Container(
              height: 50,
               decoration: BoxDecoration(
                 color: offWhite,
                   borderRadius: BorderRadius.circular(50)),

                  child: InkWell(
                    onTap: (){
                        setState(() {
                          if (isDatePicker){
                            isDatePicker = false;
                          }else{
                            isDatePicker = true;
                          }

                        });
                    },
                      child: Row(

                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          Text(strDate == null ? 'Select Year & Month' : strDate,
                            style: buttonTextStyle
                                .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),
                          Icon(Icons.keyboard_arrow_down_sharp),
                        ],
                      )
                  ),


             ),
          ),
           isDatePicker ? SizedBox(

               height: MediaQuery.of(context).size.height /2 -50,

           child:Padding(
               padding: EdgeInsets.only(left: 60,right: 60),
           child :Column(

             crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  FlatButton(
                   // color: Colors.blue,
                    textColor: Colors.black,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      setState(() {
                        isDatePicker = false;
                      });
                    },
                    child: Text(
                      "Cancel",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  ),
                  FlatButton(
                   // color: Colors.blue,
                    textColor: Colors.blue,
                    disabledColor: Colors.grey,
                    disabledTextColor: Colors.black,
                    padding: EdgeInsets.all(8.0),
                    splashColor: Colors.blueAccent,
                    onPressed: () {
                      getSalary();
                      setState(() {
                        isDatePicker = false;
                      });
                    },
                    child: Text(
                      "Done",
                      style: TextStyle(fontSize: 20.0),
                    ),
                  )
                ],
              ),
              Container(
                color: Colors.white,
                height: MediaQuery.of(context).size.height /2 - 100,
                child: CupertinoTheme(
                  data: CupertinoThemeData(
                    textTheme: CupertinoTextThemeData(
                      dateTimePickerTextStyle:
                      textFieldTextStyle.copyWith(
                          color: orangeColor),
                    ),
                  ),
                  child: CupertinoDatePicker(
                    initialDateTime: DateTime.now(),
                    onDateTimeChanged: (DateTime newdate) {
                      print(newdate);
                      setState(() {
                        final DateFormat formatter = DateFormat('MMMM-yyyy');
                        strDate = formatter.format(newdate);
                        //isDatePicker = false;
                      });
                    },
                    use24hFormat: false,
                    maximumDate: new DateTime.now(),
                    minimumYear: 2010,
                    maximumYear: year,
                    minuteInterval: 1,
                    mode: CupertinoDatePickerMode.date,
                  ),
                ),
              )
            ],
              ) ,  
           )):
               Column(
                 children: [
                   SizedBox(
                     height: 20,
                   ),
                   Container(
                    // height: 30,
                     color: darkBlue,
                     child: Stack(
                       children: [
                         Padding(
                           padding: const EdgeInsets.only(top: 20),
                           child: Image.asset('assets/icons/timer-cut.png',height: 50,width: 50,),
                         ),
                         // Icon(Icons.timer, color: offWhite, size: 70,),
                         Padding(
                           padding: const EdgeInsets.fromLTRB(50 , 20, 50, 20),
                           child: Column(
                             children: [
                               Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: [
                                   Text('Total work',
                                     style: buttonTextStyle
                                         .copyWith(color: Colors.white,fontWeight: FontWeight.w600),),
                                   Text(widget.dictParam['totalWorkHr'].toString()+':'+widget.dictParam['totalWorkMins'].toString()+'hr',
                                     style: buttonTextStyle
                                         .copyWith(color: Colors.white, fontWeight: FontWeight.w600),)
                                 ],
                               ),
                               // SizedBox(
                               //   height: 5,
                               // ),
                               // Row(
                               //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                               //   children: [
                               //     Text('Extra work hr',
                               //       style: buttonTextStyle
                               //           .copyWith(color: Colors.white, fontWeight: FontWeight.w600),),
                               //     Text('2hr',
                               //       style: buttonTextStyle
                               //           .copyWith(color: Colors.white, fontWeight: FontWeight.w600),)
                               //   ],
                               // ),
                             ],
                           ),
                         ),
                       ],
                     ),

                   ),
                   SizedBox(
                     height: 20,
                   ),
                 ],
               ),


          Container(
            color: offWhite,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(50 , 20, 50, 20),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text('Total Salary',
                        style: buttonTextStyle
                            .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),
                      Text(widget.dictParam['currency']+" "+widget.dictParam['totalSalary'].toString(),
                        style: buttonTextStyle
                            .copyWith(color: darkBlue,fontWeight: FontWeight.w600),)
                    ],
                  ),
                  // SizedBox(
                  //   height: 5,
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Text('Extra Salary Earned',
                  //       style: buttonTextStyle
                  //           .copyWith(color: darkBlue, fontWeight: FontWeight.w600),),
                  //     Text('GBP 500',
                  //       style: buttonTextStyle
                  //           .copyWith(color: darkBlue,fontWeight: FontWeight.w600),)
                  //   ],
                  // ),
                  // Padding(
                  //   padding: const EdgeInsets.only(top: 10,bottom: 5),
                  //   child: Divider(
                  //     color: Colors.grey[400],
                  //     height: 5,
                  //     indent: 0,
                  //     endIndent: 0,
                  //   ),
                  // ),
                  // Row(
                  //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  //   children: [
                  //     Text('Extra Salary Earned',
                  //       style: buttonTextStyle
                  //           .copyWith(color: darkBlue, fontWeight: FontWeight.w600),),
                  //     Text('GBP 3000',
                  //       style: buttonTextStyle
                  //           .copyWith(color: darkBlue,fontWeight: FontWeight.w600),)
                  //   ],
                  // ),
                ],

              ),
            ),
          )
        ],
      ): widget.isError ?
      Column(

        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,

        children: [
          Center(
           child: Text(
              "Oops!Something Went Wrong",
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 20.0,
                color: darkBlue,
                fontFamily: 'PoppinsRegular',
              ),
            ),
          ),

        ],

      ) : Container()
    );
  }
  getSalary()async{
    String authKey;
    String sessionKey;

    EasyLoading.show();

    final prefs = await SharedPreferences.getInstance();
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    String strUrl = WebApiConstaint.API_URL_STAFF_SALARY + '?month='+ strDate;

    provider.requestGetForApi(strUrl, authKey,sessionKey ).then((responce)  async {

      if(responce.data["error"] == false) {
        widget.dictParam = responce.data;
        this.setState(() {
          print(responce.data);
        });
      }else{

        widget.isError = true;
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);
      }
      EasyLoading.dismiss();
    });
  }

}


// new ListView.builder(
// shrinkWrap: true,
// physics: const NeverScrollableScrollPhysics(),
// itemCount: 3,
// //myList == null ? 0 : myList.length,
// itemBuilder: (BuildContext context, int indexpos) {
// //  final item = myList[indexpos];
// return new GestureDetector(
// onTap: () {
//
// },
// child:
// Container(
// color: Colors.white,
// child: Row(
// mainAxisAlignment: MainAxisAlignment.spaceBetween,
// children: [
// Text('Total Salary',
// style: buttonTextStyle
//     .copyWith(color: darkBlue),),
// Text('GBP 2500',
// style: buttonTextStyle
//     .copyWith(color: darkBlue),)
// ],
// ),
// ),
// );
// }
// ),
