import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/HomeBaseScreen.dart';
import 'package:repeat_click/ui/LoginScreen.dart';
import 'package:repeat_click/utils/strings.dart';
import 'dart:io';

import 'package:shared_preferences/shared_preferences.dart';

class SecurePinScreen extends StatefulWidget {
  @override
  SecurePinScreenState createState() => SecurePinScreenState();
}

class SecurePinScreenState extends State<SecurePinScreen> {
  TextEditingController txtEnterPin = TextEditingController();
  bool termsConditions = false;
  String strPin;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    strPin = '';
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: whiteColor,
        body: SingleChildScrollView(
          padding: EdgeInsets.fromLTRB(0, 0, 0, 20),
          child: Column(
            children: [
              new Container(
                height: 160,
                width: MediaQuery.of(context).size.width,
                color: Colors.white,
                child: Image.asset('assets/image/downCurve.png',fit: BoxFit.fill,),
              ),
              new Padding(
                padding: const EdgeInsets.all(20),
                child: new Text(
                  securePinText,
                  style: titleTextStyle,
                ),
              ),
               Container(
                 height: 70,
                padding: const EdgeInsets.fromLTRB(40, 20, 40, 0),
                child: new TextField(
                  readOnly: true,
                  controller: txtEnterPin,
                  style: textFieldTextStyle,
                  maxLength: 6,
                  maxLines: 1,
                  decoration: new InputDecoration(
                    counterText: "",
                    border: border,
                    focusedBorder: focusedBorder,
                    hintText: enterPinText,
                    hintStyle: hintStyle,
                    contentPadding: EdgeInsets.all(15),
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.fromLTRB(40, 20, 40, 0),
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "1",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('1');

                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "2",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('2');

                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "3",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('3');

                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "4",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('4');
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "5",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('5');
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "6",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('6');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "7",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('7');
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "8",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('8');
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "9",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('9');
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "<",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                openLoginScreen();
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: FlatButton(
                              splashColor: Colors.transparent,
                              highlightColor: Colors.transparent,
                              child: Text(
                                "0",
                                style: orangeRegularTextStyle,
                              ),
                              onPressed: () {
                                enterPin('0');
                              },
                            ),
                          ),
                          Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                    40.0
                                ),
                                border: Border.all(
                                    width: 2,
                                    color: orangeColor,
                                    style: BorderStyle.solid)),
                            child: Center(
                              child: FlatButton(
                                splashColor: Colors.transparent,
                                highlightColor: Colors.transparent,
                                child: Icon(
                                  Icons.backspace_rounded,
                                  color: orangeColor,
                                ),
                                onPressed: () {
                                  List<String> c = strPin.split("");
                                  c.removeLast();
                                  strPin = c.join();
                                  setState(() {
                                    txtEnterPin.text = strPin;
                                  });

                                },
                              ),
                            ),

                          ),
                        ],
                      ),
                    ),
                    new Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: new SizedBox(
                          width: double.infinity,
                          // height: double.infinity,
                          child: FlatButton(
                            height: 50,
                            child: Text(
                              loginText.toUpperCase(),
                              style: buttonTextStyle,
                            ),
                            color: orangeColor,
                            shape: shape,
                            onPressed: () {
                              openHomeScreen();
                            },
                          )),
                    ),
                  ],
                ),
              )

            ],
          ),
        ));
  }
  enterPin(String strDigit) {
    if (strPin != null) {
      if (strPin.length < 6) {
        strPin = strPin + (strDigit);
      }
    } else {
      strPin = strDigit;
    }

    setState(() {
      txtEnterPin.text = strPin;
    });
  }
    openLoginScreen (){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
      );

    }

    openHomeScreen()async{
      SharedPreferences prefs = await SharedPreferences.getInstance();
      var strTempPin = prefs.get(WebApiConstaint.kQuickPin);
      if (strTempPin == txtEnterPin.text){
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => HomeBaseScreen()),
        );
      }else{
        Fluttertoast.showToast(msg: 'Please enter correct Pin');

      }
    }

}


