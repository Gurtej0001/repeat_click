import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/utils/draw_arc_bottom.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'SetupPinScreen.dart';

class LoginScreen extends StatefulWidget {
  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  TextEditingController txtPhone = TextEditingController();
  TextEditingController txtPassword = TextEditingController();

  bool showvalue = false;
  ApiProvider provider = ApiProvider();
  FocusNode myFocusNode;
  bool termsConditions = false;
  var _paint = Paint()
    ..color = orangeColor
    ..strokeWidth = 10.0
    ..style = PaintingStyle.fill;

  @override
  void initState() {
    super.initState();

    myFocusNode = FocusNode();
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
          backgroundColor: whiteColor,

          body: GestureDetector(
              onTap: (){
                FocusScope.of(context).requestFocus(new FocusNode());
              },
              child : SingleChildScrollView(
            child: Column(
              children: [

                 Container(
                  color: Colors.white,
                  height: MediaQuery.of(context).size.height / 2 - 50, //400.0,
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset('assets/image/downCurve.png',fit: BoxFit.fill,)
                ),
                new Padding(
                  padding: const EdgeInsets.all(20),
                  child: new Text(
                    loginText,
                    style: TextStyle(fontSize: 50.0,
                        color: orangeColor,
                        fontFamily: 'PaytoneOne',fontWeight: FontWeight.w800),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top:20,left: 20,right: 20),
                  child: Column(
                    children: [
                      TextFormField(
                        controller: txtPhone,
                        textInputAction: TextInputAction.next,
                        style: textFieldTextStyle,
                        maxLength: 11,
                        maxLines: 1,
                        decoration: new InputDecoration(
                          counterText: "",
                          border: border,
                          focusedBorder: focusedBorder,
                          hintText: phoneText,
                          hintStyle: hintStyle,
                          contentPadding: EdgeInsets.all(15),
                        ),
                        keyboardType: TextInputType.number,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      new TextFormField(
                        controller: txtPassword,
                        style: textFieldTextStyle,

                        maxLength: 15,
                        maxLines: 1,
                        obscureText: true,
                        decoration: new InputDecoration(
                            counterText: "",
                            border: border,
                            focusedBorder: focusedBorder,
                            hintText: passwordText,
                            hintStyle: hintStyle,
                            contentPadding: EdgeInsets.all(15)
                        ),
                        textInputAction: TextInputAction.done,
                      ),

                    ],
                  ),
                ),

                new Padding(
                  padding: const EdgeInsets.fromLTRB(30, 20, 30, 0),
                  child: new SizedBox(
                      width: double.infinity,
                      // height: double.infinity,
                      child: FlatButton(
                        height: 50,
                        child: Text(
                          continueText.toUpperCase(),
                          style: buttonTextStyle,
                        ),
                        color: orangeColor,
                        shape: shape,
                        onPressed: () {
                          if (txtPhone.text == ""){
                            Fluttertoast.showToast(msg: 'Please Enter Phone number');
                           }else if (txtPhone.text.length <11){
                             Fluttertoast.showToast(msg: 'Please enter 11 digits of Phone number');
                          }else if (txtPassword.text == ""){
                            Fluttertoast.showToast(msg: 'Please enterPassword');
                          }else if (txtPassword.text.length < 6 ){
                             Fluttertoast.showToast(msg: 'Please enter atleast 6 digits Password');
                          }else if (!termsConditions){
                            Fluttertoast.showToast(msg: 'Please accept terms & conditions');
                          } else{
                             // openSetPinScreen();
                             loginApi(context);
                          }

                        },
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  child: Theme(
                    data: ThemeData(unselectedWidgetColor: orangeColor),
                    child: CheckboxListTile(
                      contentPadding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                      checkColor: Colors.white,
                      activeColor: orangeColor,
                      title: Text(
                        termsConditionsText,
                        style: orangeRegularTextStyle,
                      ),
                      value: this.termsConditions,
                      onChanged: (bool value) {
                        setState(() {
                          this.termsConditions = value;
                        });
                      },
                      controlAffinity:
                      ListTileControlAffinity.leading, //  <-- leading Checkbox
                    ),
                  ),
                )
              ],
            ),
          )),)

    );

  }

  void setString(String authenticationKey, String sessionKey , Map<dynamic,dynamic> dictData) async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.containsKey(WebApiConstaint.kAuthKey)) {
      prefs.remove(WebApiConstaint.kAuthKey);
    }
    prefs.setString(WebApiConstaint.kSessionKey, sessionKey);
    prefs.setString(WebApiConstaint.kAuthKey, authenticationKey);
    prefs.setString(WebApiConstaint.kUserData, json.encode(dictData));
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SetupPinScreen(isChangePassword: false,)),
    );
    print(authenticationKey);
  }

  loginApi(BuildContext context) async {
    EasyLoading.show();
    Map<String, String> dictParam = {'mobile_number': txtPhone.text, 'password': txtPassword.text,};
    print(dictParam);
    provider.requestPostForApi(dictParam, WebApiConstaint.API_URL_LOGIN, "","").then((responce) async {
      if (responce.data["error"] == false) {
        print(responce.data["message"]);
        if (responce.data["errorCode"] == 0) {
          var data = responce.data["data"];
          setString(data["authorization"], data["session_key"], data);
          print(data["authorization"]);
        }
        else {
          Fluttertoast.showToast(
              msg: responce.data["message"]);
          print(responce.data["message"]);
        }
        EasyLoading.dismiss();
      } else {
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);
        EasyLoading.dismiss();
      }
    }
    );
  }

  openSetPinScreen(){
    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => SetupPinScreen(isChangePassword: false,)),
    );
  }
  Future<bool> _onWillPop() async {
    return (await showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () => SystemNavigator.pop(),
            child: new Text('Yes'),
          ),
        ],
      ),
    )) ??
        false;
  }
}
