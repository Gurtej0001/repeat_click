import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';

class NotificationScreen extends StatefulWidget {
  @override
  _NotificationScreenState createState() => _NotificationScreenState();
}

class _NotificationScreenState extends State<NotificationScreen> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: new AppBar(
        //automaticallyImplyLeading: false,
        elevation: 0.0,
        title: Text(
          'Notification',
          style: TextStyle(
            fontSize: 20.0,
            color: whiteColor,
            fontFamily: 'PoppinsRegular',
          ),
        ),
        // actions: <Widget>[
        //   new IconButton(
        //     icon: new Icon(Icons.notification_important_outlined),
        //     onPressed: () {
        //       Navigator.pop(context);
        //     },
        //   ),
        // ],
      ),
      body: ListView.builder(
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 3,
          //myList == null ? 0 : myList.length,
          itemBuilder: (BuildContext context, int indexpos){
            return Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.only(left: 10,top:10,right: 10),
              child: Column(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(child: Text("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.",style: normal14TextStyle
                          .copyWith(color: darkBlue),maxLines: 4,)),

                      SizedBox(width: 10),
                      Text('2003-Apr-02',
                        style: normal14TextStyle
                            .copyWith(color: Colors.grey),)
                    ],
                  ),
                  Divider(
                    color: Colors.grey,
                    height: 5,
                   // indent: 60,
                  ),
                ],
              ),

            );
          }

      ),
    );
  }
}
