import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/Model/shift.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';


class ShiftScreen extends StatefulWidget {
  Map<dynamic, dynamic> dictParam;
  bool isError = false;
  ShiftScreen({Key key, @required this.dictParam,this.isError}) : super(key: key);
  @override
  ShiftScreenState createState() => ShiftScreenState();
}

class ShiftScreenState extends State<ShiftScreen> {
  Map<dynamic,dynamic> dictTempParam;
  ApiProvider provider = ApiProvider();
  // Map<dynamic, dynamic> dictProfile;
  List<ShiftModel> myList = new List();
  String authKey;
  String sessionKey;

  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey(WebApiConstaint.kUserData);
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    if (checkValue){
      dictTempParam = json.decode(prefs.getString(WebApiConstaint.kUserData));
      print(dictTempParam);
    }
  }

  @override
  Widget build(BuildContext context) {
    if (widget.isError == null){
      widget.isError = false;
    }

    getUserData();
    DateTime now = DateTime.now();
    String formattedDate = DateFormat("dd-MM-yyyy hh:mm a").format(now);
    if (widget.dictParam != null){
      myList.clear();
      for (var i in widget.dictParam['data']) {
        var data = ShiftModel.fromJson(i);
        myList.add(data);
        print(myList);
        print(widget.dictParam['totalWorkHr'].toString()+':'+widget.dictParam['totalWorkMins'].toString()+"hr");
      }
    }
    return Scaffold(
        body: widget.dictParam != null && dictTempParam != null ?SingleChildScrollView(
          child: Column(

            children: [
              Container(
                height: 240.0,
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                      color: orangeColor,
                      width: MediaQuery.of(context).size.width,
                      height: 160.0,
                      child: Column(
                        children: [
                          Text(
                            dictTempParam['designation'],
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 16.0,
                              color: whiteColor,
                              fontFamily: 'PoppinsRegular',
                            ),
                          ),
                        ],
                      ),
                    ),
                    Positioned(
                      top: 85,
                      left: 100,
                      right: 100,
                      child:dictTempParam != null && dictTempParam['image'] != '' ? Container(
                        decoration: BoxDecoration(
                          color: Colors.white,
                          shape: BoxShape.circle,
                          boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey[800], spreadRadius: 1.5)],
                        ),
                        child:  CircleAvatar(
                            radius: 50,
                            // backgroundColor: Color(0xffFDCF09),


                            child:  ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Image.network(
                                  dictTempParam['image'],
                                  width: 100,
                                  height: 100,
                                  fit: BoxFit.cover,
                                )
                            )
                        ) ,
                      ):Container(
                        width: 100.0,
                        height: 100.0,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                            boxShadow: [BoxShadow(blurRadius: 5, color: Colors.black, spreadRadius: 2)],
                            image: new DecorationImage(
                                fit: BoxFit.fitHeight,
                                image: AssetImage('assets/image/plcaeholder.png')
                            )
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              widget.dictParam['status'] != 1 ?Container(
                padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                child: Column(
                  children: [
                    new Text(
                      formattedDate,
                      textAlign: TextAlign.center,
                      style: normal16TextStyle,
                    ),
                    Visibility(
                      visible: widget.dictParam['status'] == 2,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FlatButton(
                              height: 50,
                              minWidth: 150,
                              child: Text(
                                startBreakText,
                                style: buttonTextStyle,
                              ),
                              color: orangeColor,
                              shape: shape,
                              onPressed: () {
                                staffAttendence('bi');
                              },
                            ),
                            FlatButton(
                              height: 50,
                              minWidth: 150,
                              child: Text(
                                EndShiftText,
                                style: buttonTextStyle,
                              ),
                              color: darkBlue,
                              shape: shape,
                              onPressed: () {
                                staffAttendence('ou');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Visibility(
                      visible: widget.dictParam['status'] == 3,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FlatButton(
                              height: 50,
                              minWidth: 150,
                              child: Text(
                                endBreakText,
                                style: buttonTextStyle,
                              ),
                              color: orangeColor,
                              shape: shape,
                              onPressed: () {
                                staffAttendence('bo');
                              },
                            ),
                            FlatButton(
                              height: 50,
                              minWidth: 150,
                              child: Text(
                                EndShiftText,
                                style: buttonTextStyle,
                              ),
                              color: darkBlue,
                              shape: shape,
                              onPressed: () {
                                staffAttendence('ou');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Visibility(
                      visible: widget.dictParam['status'] == 4,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            FlatButton(
                              height: 50,
                              minWidth: 150,
                              child: Text(
                                EndShiftText,
                                style: buttonTextStyle,
                              ),
                              color: darkBlue,
                              shape: shape,
                              onPressed: () {
                                staffAttendence('ou');
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                    Visibility(
                      visible: widget.dictParam['status'] == 5,
                      child: Padding(
                        padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text('Your shift is completed for today',
                              style: buttonTextStyle
                                  .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),

                          ],
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 20.0, left: 20.0, right: 20.0),
                      child: Container(
                        height: MediaQuery.of(context).size.height/3 - 30,
                        width: MediaQuery.of(context).size.width - 60,
                        decoration: BoxDecoration(
                            border: Border.all(),
                            borderRadius: BorderRadius.all(Radius.circular(5))
                        ),
                        child:
                        Padding(
                          padding: const EdgeInsets.only(left: 10,right: 10,top: 10,bottom: 10),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              Text("IN PROGRESS", style: normal18TextStyle.copyWith(color: orangeColor, fontWeight: FontWeight.w600),),
                              SizedBox(
                                height: 10,
                              ),
                              Expanded(child: new ListView.builder(
                                  shrinkWrap: true,
                                  physics: const NeverScrollableScrollPhysics(),
                                  itemCount: myList == null ? 0 : myList.length,
                                  //myList == null ? 0 : myList.length,
                                  itemBuilder: (BuildContext context, int indexpos) {
                                    //  final item = myList[indexpos];
                                    final item = myList[indexpos];
                                    return new GestureDetector(
                                      onTap: () {

                                      },
                                      child: Container(
                                        color: Colors.white,
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(myList[indexpos].title,
                                              style: normal14TextStyle
                                                  .copyWith(color: darkBlue),),
                                            Text(myList[indexpos].time,
                                              style: normal14TextStyle
                                                  .copyWith(color: Colors.grey),)
                                          ],
                                        ),
                                      ),

                                    );
                                  }
                              ),),

                              Divider(
                                color: Colors.grey[400],
                                height: 5,
                                indent: 0,
                                endIndent: 0,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text('Total Work',
                                    style: normal14TextStyle
                                        .copyWith(color: darkBlue),),
                                  Text(widget.dictParam['totalWorkHr'].toString()+':'+widget.dictParam['totalWorkMins'].toString()+"hr",
                                    style: normal14TextStyle
                                        .copyWith(color: Colors.grey),)
                                ],
                              // ),
                              // Row(
                              //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              //   children: [
                              //     Text('Extra Work hrs',
                              //       style: normal14TextStyle
                              //           .copyWith(color: darkBlue),),
                              //     Text('2hr',
                              //       style: normal14TextStyle
                              //           .copyWith(color: Colors.grey),)
                              //   ],
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ) : Center(
                child: Text('Please start your shift to get the details',
                  style: buttonTextStyle
                      .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),
              ),
            ],
          )
        ):widget.isError ?
        Column(

          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Center(
              child: Text(
                "Oops!Something Went Wrong",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  color: darkBlue,
                  fontFamily: 'PoppinsRegular',
                ),
              ),
            ),



          ],

        ) : Container(),);
  }
  staffAttendence(String strAttendenceType )async{

    EasyLoading.show();
    Map<String, String> dictParam = {
      'type': strAttendenceType,
      'latitude': '26.880050',
      'longitude': '75.764060',
    };
    print(dictParam);
    provider.requestPostForApi(dictParam, WebApiConstaint.API_URL_ATTENDENCE, authKey, sessionKey)..then((responce) async{
      if(responce.data["error"] == false){
        EasyLoading.dismiss();
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        widget.dictParam['status'] = responce.data["status"];
        getShift();

      }
      else{
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);

      }
      EasyLoading.dismiss();
    });

  }

  getShift(){
    String strUrl = WebApiConstaint.API_URL_GET_SHIFT;
    EasyLoading.show();
    provider.requestGetForApi(strUrl, authKey,sessionKey ).then((responce)  async {

      if(responce.data["error"] == false) {

        widget.dictParam = responce.data;

        setState(() {

        });
      }else{

        setState(() {

        });
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);
      }
      EasyLoading.dismiss();
    });
  }

}

