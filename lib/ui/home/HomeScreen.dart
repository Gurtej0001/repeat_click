import 'dart:async';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:repeat_click/ui/QrCodeScannerScreen.dart';
import 'package:repeat_click/utils/strings.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';


class HomeScreen extends StatefulWidget {
  int btnStartEndState = 1;
  HomeScreen({Key key, @required this.btnStartEndState}) : super(key: key);
  @override
  HomeScreenState createState() => HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {

  LatLng _center ;
  Position currentLocation;

  LatLng _initialCameraPosition = LatLng(26.880050, 75.764060);
  GoogleMapController _controller;
 // Location _location = Location();
  final List<Marker> markerList = [];
  Map<dynamic,dynamic> dictParam;
  String strName = "";

  BitmapDescriptor customIcon1;
  LatLng pinPosition = LatLng(37.3797536, -122.1017334);
  BitmapDescriptor pinLocationIcon;
  Completer<GoogleMapController> _controller1 = Completer();
  Set<Marker> _markers = {};
  ApiProvider provider = ApiProvider();
  double latitude;
  double longitude;
  String authKey;
  String sessionKey;
  CameraPosition initialLocation = CameraPosition(
      zoom: 16,
      bearing: 30,
      target: LatLng(37.3797536, -122.1017334),
  );


  @override
  void initState() {
    super.initState();

    getUserLocation();
    BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/marker.png').then((onValue) {
      pinLocationIcon = onValue;
    });
    setCustomMapPin();
  }

  void setCustomMapPin() async {
    pinLocationIcon = await BitmapDescriptor.fromAssetImage(
        ImageConfiguration(devicePixelRatio: 2.5),
        'assets/image/marker.png');
  }

  Future<Position> locateUser() async {
    return Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  }

  getUserLocation() async {
    currentLocation = await locateUser();
    setState(() {
      _center = LatLng(currentLocation.latitude, currentLocation.longitude);
      latitude = currentLocation.latitude;
      longitude = currentLocation.longitude;
    });

    print('center $_center');
  }

  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey(WebApiConstaint.kUserData);
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    if (checkValue){
      dictParam = json.decode(prefs.getString(WebApiConstaint.kUserData));
      print(dictParam);
    }
    if (dictParam != null){
      strName = dictParam['name'];
    }
  }

  @override
  Widget build(BuildContext context) {

    DateTime now = DateTime.now();
    String formattedDate = DateFormat("dd-MM-yyyy hh:mm a").format(now);
    getUserData();

    Set<Circle> circles = Set.from([Circle(
      circleId: CircleId('1'),
      center: pinPosition,
      radius: 4000,
    )]);

    CameraPosition initialLocation = CameraPosition(
        zoom: 16,
        bearing: 30,
        target: pinPosition,
    );

    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: [
         dictParam != null ? Container(
           height: MediaQuery.of(context).size.height * 0.30,
          // width: MediaQuery.of(context).size.width,
           // height: 240.0,
            child: Stack(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(10, 20, 10, 0),
                  color: orangeColor,
                  width: MediaQuery.of(context).size.width,
                  height: 160.0,
                  child: Column(
                    children: [
                      Text(
                        "Welcome to work " + strName+". Have a great day ahead.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16.0,
                          color: whiteColor,
                          fontFamily: 'PoppinsRegular',
                        ),
                      ),
                    ],
                  ),
                ),
                Positioned(
                    top: 85,
                    left: 100,
                    right: 100,
                  child:dictParam != null && dictParam['image'] != '' ? Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        shape: BoxShape.circle,
                        boxShadow: [BoxShadow(blurRadius: 3, color: Colors.grey[800], spreadRadius: 1.5)],
                      ),
                    child:  CircleAvatar(
                      radius: 50,
                     // backgroundColor: Color(0xffFDCF09),


                      child:  ClipRRect(
                          borderRadius: BorderRadius.circular(50),
                          child: Image.network(
                            dictParam['image'],
                            width: 100,
                            height: 100,
                            fit: BoxFit.cover,
                          )
                      )
                      // ) :Container(
                      //     decoration: BoxDecoration(
                      //         color: Colors.grey[200],
                      //         borderRadius: BorderRadius.circular(50)),
                      //     width: 100,
                      //     height: 100,
                      //     child: Image.asset('assets/images/plcaeholder.png')
                      // ),
                    ) ,
                  ):Container(
                    width: 100.0,
                    height: 100.0,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle,
                      boxShadow: [BoxShadow(blurRadius: 5, color: Colors.black, spreadRadius: 2)],
                      image: new DecorationImage(
                          fit: BoxFit.fitHeight,
                          image: AssetImage('assets/image/plcaeholder.png')
                      )
                    ),

                  ),
                )
              ],
            ),
          ):SizedBox(height: 5,),
          Container(
            height: MediaQuery.of(context).size.height * 0.70,
            padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
            child: Column(
              //mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                new Text(
                  formattedDate,
                  textAlign: TextAlign.center,
                  style: normal16TextStyle,
                ),
                Visibility(
                  visible: widget.btnStartEndState == 1,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        FlatButton(
                          minWidth: 150,
                          height: 50,
                          child: Text(
                            startShiftText,
                            style: buttonTextStyle,
                          ),
                          color: orangeColor,
                          shape: shape,
                          onPressed: () {

                            // Map<String, String> dictParam = {
                            //   'type': 'in',
                            //   'latitude': latitude.toString(),
                            //   'longitude': longitude.toString(),
                            // };
                            openQrScanner();
                          },
                        ),


                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.btnStartEndState == 2 ||widget.btnStartEndState == 3 ||widget.btnStartEndState == 4,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [

                        FlatButton(
                          height: 50,
                          minWidth: 150,
                          child: Text(
                            EndShiftText,
                            style: buttonTextStyle,
                          ),
                          color: darkBlue,
                          shape: shape,
                          onPressed: () {
                            staffAttendence('ou');
                          },
                        ),
                      ],
                    ),
                  ),
                ),
                Visibility(
                  visible: widget.btnStartEndState == 5,
                  child: Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child: new Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                          Text('Your shift is completed for today',
                      style: buttonTextStyle
                          .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),

                      ],
                    ),
                  ),
                ),
                Padding(
                    padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                    child : Container(
                      height: MediaQuery.of(context).size.height *0.40,
                      width: MediaQuery.of(context).size.width,
                    child: Stack(
                      children: [
                         GoogleMap(
                           markers: _markers,
                           initialCameraPosition: initialLocation,
                            mapType: MapType.normal,
                          //  onMapCreated: _onMapCreated,
                            onMapCreated: (GoogleMapController controller) {
                              _controller1.complete(controller);
                              setState(() {
                                _markers.add(
                                    Marker(
                                        markerId: MarkerId("1"),
                                        position: pinPosition,
                                        icon: pinLocationIcon
                                    )
                                );
                              });
                            },
                           circles: circles,
                           myLocationEnabled: true,
                  /*   markers: markerList.toSet(),
              onTap: (coordinate) {
                _controller.animateCamera(CameraUpdate.newLatLng(coordinate));
                addMarker(coordinate);
              },*/
                ),
              ],
            ),
          ),
                )

              ],
            ),
          ),
        ],
      ),
    ));
  }

  // void _onMapCreated(GoogleMapController _cntlr) {
  //   _controller = _cntlr;
  //   _location.onLocationChanged.listen((l) {
  //     _controller.animateCamera(
  //       CameraUpdate.newCameraPosition(
  //         CameraPosition(target: LatLng(l.latitude, l.longitude), zoom: 20),
  //       ),
  //     );
  //   });
  // }

  void addMarker(coordinate) {
    int id = Random().nextInt(100);
    setState(() {
      markerList.add(Marker(
          position: coordinate,
          // position: LatLng(26.880030, 75.764060),
          infoWindow:
          InfoWindow(title: "${"marker name"}", snippet: "${"shop"}"),
          markerId: MarkerId(id.toString())));
    });
  }

  createMarker(context) {
    if (customIcon1 == null) {
      ImageConfiguration configuration = createLocalImageConfiguration(context);
      BitmapDescriptor.fromAssetImage(configuration, 'assets/image/logo.png')
          .then((icon) {
        setState(() {
          customIcon1 = icon;
        });
      });
    }
  }
  void onGoBack(dynamic value) {

    setState(() {});
  }

  void openQrScanner() async {

    final result = await Navigator.push(
      context,
      MaterialPageRoute(
          fullscreenDialog: true, builder: (context) => QrCodeScannerScreen()),
    );
    //
    if (result == dictParam['qrCode']) {
      staffAttendence('in');

    }else{
      Fluttertoast.showToast(
          msg: "Invalid qrcode");
    }
  }

  staffAttendence(String strAttendenceType )async{

    EasyLoading.show();
    Map<String, String> dictParam = {
      'type': strAttendenceType,
      'latitude': latitude.toString(),
      'longitude': longitude.toString(),
    };
    print(dictParam);
    provider.requestPostForApi(dictParam, WebApiConstaint.API_URL_ATTENDENCE, authKey, sessionKey)..then((responce) async{
      if(responce.data["error"] == false){
        EasyLoading.dismiss();
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        widget.btnStartEndState = responce.data['status'];
        setState(() {

        });

      }
      else{
        Fluttertoast.showToast(
            msg: responce.data["message"]);
        print(responce.data["message"]);

      }
      EasyLoading.dismiss();
    });

  }

}
