import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:shared_preferences/shared_preferences.dart';

class QrCodeScannerScreen extends StatefulWidget {
  @override
  _QrCodeScreen createState() => _QrCodeScreen();
}

class _QrCodeScreen extends State<QrCodeScannerScreen> {
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController controller;
  Barcode barCode;
  Map<dynamic,dynamic> dictParam;
  ApiProvider provider = ApiProvider();
  String authKey;
  String sessionKey;

  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    bool checkValue = prefs.containsKey(WebApiConstaint.kUserData);
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    if (checkValue){
      dictParam = json.decode(prefs.getString(WebApiConstaint.kUserData));
      print(dictParam);
    }

  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _buildQrView(context),
    );
  }

  Widget _buildQrView(BuildContext context) {
    getUserData();
    // For this example we check how width or tall the device is and change the scanArea and overlay accordingly.
    var scanArea = (MediaQuery.of(context).size.width < 400 ||
            MediaQuery.of(context).size.height < 400)
        ? 150.0
        : 300.0;
    // To ensure the Scanner view is properly sizes after rotation
    // we need to listen for Flutter SizeChanged notification and update controller
    return NotificationListener<SizeChangedLayoutNotification>(
        onNotification: (notification) {
          Future.microtask(
              () => controller?.updateDimensions(qrKey, scanArea: scanArea));
          return false;
        },
        child: SizeChangedLayoutNotifier(
            key: const Key('qr-size-notifier'),
            child: QRView(
              key: qrKey,
              onQRViewCreated: _onQRViewCreated,
              overlay: QrScannerOverlayShape(
                borderColor: Colors.red,
                borderRadius: 10,
                borderLength: 30,
                borderWidth: 10,
                cutOutSize: scanArea,
              ),
            )));
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    bool isScanned = false;
    controller.scannedDataStream.listen((scanData) {
      setState(() {
        print(scanData);
        barCode = scanData;
        if (!isScanned){
          Navigator.of(context).pop(scanData.code);
        }
        isScanned = true;

      });

    });

  }

}
