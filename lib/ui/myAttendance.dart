
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_calendar_carousel/classes/event.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:repeat_click/API/api_provider.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/Font/font_Style.dart';
import 'package:flutter_calendar_carousel/flutter_calendar_carousel.dart';
import 'package:intl/intl.dart';
import 'package:repeat_click/Model/attendanceModel.dart';
import 'package:shared_preferences/shared_preferences.dart';



class AttendanceScreen extends StatefulWidget {
  Map<dynamic, dynamic> dictParam;
  bool isSelect;
  AttendanceScreen({Key key, @required this.dictParam, @required this.isSelect}) : super(key: key);
  @override
  _AttendanceScreenState createState() => _AttendanceScreenState();

  



}

class _AttendanceScreenState extends State<AttendanceScreen> {
  DateTime _currentDate = DateTime.now();
  String _currentMonth = DateFormat.yMMM().format(DateTime(2021, 1, 14));
  DateTime _targetDateTime = DateTime(2019, 2, 3);
  ApiProvider provider = ApiProvider();
  List<AttendanceModel> arrAttendanceList = new List();
  String authKey;
  String sessionKey;
  String strDate;
  int year;
  int month;
  String yyear = DateFormat.y().format(DateTime.now());
  String mmonth = DateFormat.M().format(DateTime.now());
  String totalWorkDays;
  String totalWorkHr;
  String totalWorkMins;


  static Widget _eventOrangeIcon = CircleAvatar(
    radius: 100,
    backgroundColor: orangeColor,
  );

  static Widget _eventBlueIcon = CircleAvatar(
    radius: 100,
    backgroundColor: darkBlue,
  );

  getUserData() async{
    final prefs = await SharedPreferences.getInstance();
    authKey = prefs.get(WebApiConstaint.kAuthKey);
    print(authKey);
    sessionKey = prefs.get(WebApiConstaint.kSessionKey);
    print(sessionKey);

    var currDt = DateTime.now();
    var strYear = currDt.year;
    year = strYear.toInt();
    final DateFormat formatter = DateFormat('MMMM-yyyy');
    strDate = formatter.format(currDt);
  }

  getAttendance()  {
    EasyLoading.show();

    String strUrl = WebApiConstaint.API_URL_STAFF_ATTENDENCE + '?month='+ strDate;
    provider.requestGetForApi( strUrl, authKey ,sessionKey).then((responce)  async{
      if(responce.data["error"] == false){
        print(responce.data['data']);
        totalWorkDays = responce.data['totalWorkingDays'].toString();
        totalWorkHr = responce.data['totalWorkHr'].toString();
        totalWorkMins = responce.data['totalWorkMins'].toString();
        var arrTempData = responce.data['data'];
        arrAttendanceList.clear();
        _markedDateMap.clear();
        for (var i in arrTempData) {
          var data = AttendanceModel.fromJson(i);

          arrAttendanceList.add(data);
        }
        _markedDateMap.clear();
        for (var i in arrAttendanceList){
          if (i.arrive == "f") {
            _markedDateMap.add(

                new DateTime(year, month, i.date),
                new Event(
                  date: new DateTime(year, month, i.date),
                  title: '',
                  icon: _eventOrangeIcon,
                ));
          }else{
            _markedDateMap.add(

                new DateTime(year, month, i.date),
                new Event(
                  date: new DateTime(year, month, i.date),
                  title: '',
                  icon: _eventBlueIcon,
                ));
          }
        }

        this.setState(() {
          print(responce.data["message"]);
        });

      }else{
        print(responce.data["message"]);

      }
      EasyLoading.dismiss();
    });
  }

  getCalendarData(){

    arrAttendanceList.clear();
    print(widget.dictParam['data']);
    var arrTempData = widget.dictParam['data'];
    for (var i in arrTempData) {
      var data = AttendanceModel.fromJson(i);

      arrAttendanceList.add(data);
    }

    year = int.parse(yyear);
    month = int.parse(mmonth);
    _markedDateMap.clear();
    setState(() {

      for (var i in arrAttendanceList){
        if (i.arrive == "f") {
          _markedDateMap.add(

              new DateTime(year, month, i.date),
              new Event(
                date: new DateTime(year, month, i.date),
                title: '',
                icon: _eventOrangeIcon,
              ));
        }else{
          _markedDateMap.add(

              new DateTime(year, month, i.date),
              new Event(
                date: new DateTime(year, month, i.date),
                title: '',
                icon: _eventBlueIcon,
              ));
        }
      }
    });
    widget.dictParam.clear();
  }

  EventList<Event> _markedDateMap = new EventList<Event>(
    events: {
      new DateTime.now(): [
        new Event(
          date: new DateTime.now(),
          title: '',
          icon: _eventBlueIcon,
          dot: Container(
            margin: EdgeInsets.symmetric(horizontal: 1.0),
            color: Colors.red,
            height: 20.0,
            width: 20.0,
          ),
        ),
      ],
    },
  );

  @override
  void initState() {
    /// Add more events to _markedDateMap EventList
    super.initState();
<<<<<<< HEAD
=======

    getUserData();
>>>>>>> b35e47432b4fce23e7ebbddf6b12f062c4f44cd2
  }


  @override
  Widget build(BuildContext context) {
    getUserData();
    print(widget.dictParam);
    if (widget.isSelect == true){
       widget.isSelect = false;
      final DateFormat formatter = DateFormat('MMMM-yyyy');
      _currentMonth = formatter.format(DateTime.now());
       yyear = DateFormat.y().format(DateTime.now());
       mmonth = DateFormat.M().format(DateTime.now());
     }

    if (widget.dictParam != null){
      if (widget.dictParam.length > 0){
        totalWorkDays = widget.dictParam['totalWorkingDays'].toString();
        totalWorkHr = widget.dictParam['totalWorkHr'].toString();
        totalWorkMins = widget.dictParam['totalWorkMins'].toString();
        getCalendarData();
      }
    }

    CalendarCarousel _calendarCarousel, _calendarCarouselNoHeader;


    _calendarCarousel = CalendarCarousel<Event>(
      onDayPressed: (DateTime date, List<Event> events) {
        this.setState(() => _currentDate = date);
        events.forEach((event) => print(event.title));
      },

      weekendTextStyle: TextStyle(
        color: Colors.yellow,
      ),
      thisMonthDayBorderColor: Colors.transparent,
//          weekDays: null, /// for pass null when you do not want to render weekDays
      headerText: _currentMonth,
      showHeader: true,
      daysHaveCircularBorder: true,
      weekDayFormat: WeekdayFormat.narrow,
      showHeaderButton: true,
      headerTitleTouchable: false,
      //weekDayBackgroundColor: orangeColor,
      weekFormat: false,
      markedDatesMap: _markedDateMap,
      height: 310.0,
      selectedDateTime: _currentDate,
      showIconBehindDayText: true,

      iconColor: Colors.white,
      onCalendarChanged: (DateTime date) {
        this.setState(() {
          _targetDateTime = date;
          yyear = DateFormat.y().format(date);
          mmonth = DateFormat.M().format(date);
         year = int.parse(yyear);
         month = int.parse(mmonth);

          final DateFormat formatter = DateFormat('MMMM-yyyy');
          _currentMonth = formatter.format(_targetDateTime);
          strDate = _currentMonth;
          getAttendance();
        });
      },

//          daysHaveCircularBorder: false, /// null for not rendering any border, true for circular border, false for rectangular border
      customGridViewPhysics: NeverScrollableScrollPhysics(),
      markedDateShowIcon: true,
      markedDateCustomTextStyle: TextStyle(color: Colors.white),
      markedDateIconMaxShown: 2,
      selectedDayTextStyle: TextStyle(
        color: Colors.yellow,
      ),
      todayTextStyle: TextStyle(
        color: Colors.blue,
      ),
      markedDateIconBuilder: (event) {
        return event.icon;
      },
      minSelectedDate: _currentDate.subtract(Duration(days: 360)),
      maxSelectedDate: _currentDate.add(Duration(days: 360)),
      todayButtonColor: Colors.transparent,
      todayBorderColor: darkBlue,
      markedDateMoreShowTotal:
      true,
    );

    return Scaffold(
        body: widget.dictParam != null ?
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          //crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 100,
              color: darkBlue,
              child:  Padding(
                padding: const EdgeInsets.only(left: 10,top: 20,right: 20,bottom: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                     Icon(Icons.timer, color: offWhite, size: 70,),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.baseline,
                      textBaseline: TextBaseline.alphabetic,
                      children: [
                         Text('Attendance',
                          style: buttonTextStyle
                              .copyWith(color: offWhite,fontWeight: FontWeight.w500),),
                        SizedBox(
                          width: 5,
                        ),
                        Text(totalWorkDays,
                          style: buttonTextStyle
                              .copyWith(color: offWhite,fontWeight: FontWeight.w600,fontSize: 35),),
                        SizedBox(
                          width: 5,
                        ),
                        Text('Days',
                          style: buttonTextStyle
                              .copyWith(color: offWhite,fontWeight: FontWeight.w500),)
                      ],
                    ),
                  ],
                ),
              ),

            ),
            Container(
           //   height: MediaQuery.of(context).size.height/3 + 50,
              width: MediaQuery.of(context).size.width - 50,
              margin: EdgeInsets.symmetric(horizontal: 16.0),
              child: _calendarCarousel,
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20,right: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                CircleAvatar(
                radius: 5,
                backgroundColor: darkBlue,
               ),
                  SizedBox(
                    width: 5,
                  ),
                Text("Arrived", style: normal12TextStyle,),
                  SizedBox(
                    width: 10,
                  ),
                CircleAvatar(
                  radius: 5,
                  backgroundColor: orangeColor,
                ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Leave", style: normal12TextStyle,),
                  SizedBox(
                    width: 10,
                  ),
                // CircleAvatar(
                //   radius: 5,
                //   backgroundColor: Colors.red,
                // ),
                //   SizedBox(
                //     width: 5,
                //   ),
                //   Text("Leave", style: normal12TextStyle,),
                ],
              ),
            ),
          ],
        ) : Column(

          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Center(
              child: Text(
                "Oops!Something Went Wrong",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontSize: 20.0,
                  color: darkBlue,
                  fontFamily: 'PoppinsRegular',
                ),
              ),
            ),



          ],

        ),
            bottomNavigationBar:  Container(
              height: 50,
              color: offWhite,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(50 , 10, 50, 10),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Total Work',
                          style: buttonTextStyle
                              .copyWith(color: darkBlue,fontWeight: FontWeight.w600),),
                        Text(totalWorkHr +':'+ totalWorkMins +"hr",
                          style: buttonTextStyle
                              .copyWith(color: darkBlue,fontWeight: FontWeight.w600),)
                      ],
                    ),
                    // SizedBox(
                    //   height: 5,
                    // ),
                    // Row(
                    //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    //   children: [
                    //     Text('Extra Work hrs',
                    //       style: buttonTextStyle
                    //           .copyWith(color: darkBlue, fontWeight: FontWeight.w600),),
                    //     Text('2 hrs',
                    //       style: buttonTextStyle
                    //           .copyWith(color: darkBlue,fontWeight: FontWeight.w600),)
                    //   ],
                    // ),
                  ],

                ),
              ),
            ),
    );
  }
}
