import 'dart:ui';

import 'package:flutter/cupertino.dart';

class MyCustomClipper extends CustomClipper<Path> {
  final double extent;
  final double height;
  final double angledheight;

  MyCustomClipper({this.extent,this.height,this.angledheight});

  Path getClip(Size size) {
    var path = Path();
    path.moveTo(0, 0);
    path.lineTo(extent, 0);
    path.lineTo(size.width, 0);
    path.lineTo(size.width, angledheight);
    path.lineTo(00, height);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return true;
  }
}

class StripsWidget extends StatelessWidget {
  final Color color1;
  final double height;
  final double angledheight;

  const StripsWidget({Key key, this.color1,this.height,this.angledheight})
      : super(key: key);

  List<Widget> getListOfStripes() {
    List<Widget> stripes = [];
      stripes.add(
        ClipPath(
          child: Container(height:height,color: color1),
          clipper: MyCustomClipper(extent: 0,height: height,angledheight:angledheight ),
        ),
      );
    return stripes;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: getListOfStripes());
  }
}