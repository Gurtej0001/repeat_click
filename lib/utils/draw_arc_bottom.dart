import 'dart:ffi';

import 'package:flutter/cupertino.dart';

class DrawArc extends CustomPainter {
  Paint _paint;
  double _left;
  double _top;
  double _right;
  double _bottem;

  DrawArc(Paint paint, double left, double top, double right, double bottem) {
    this._paint = paint;
    this._left = left;
    this._top = top;
    this._right = right;
    this._bottem = bottem;
  }

  @override
  void paint(
    Canvas canvas,
    Size size,
  ) {
    canvas.drawArc(
        new Rect.fromLTRB(_left, _top, _right, _bottem), 50, 30, true, _paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return false;
  }
}
