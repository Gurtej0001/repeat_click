//
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:repeat_click/ui/home/HomeScreen.dart';
// import 'package:repeat_click/ui/home/ShiftScreen.dart';
// import 'package:repeat_click/ui/mySalary.dart';
//
//
// class FragmentContainer extends StatefulWidget {
//   static const String ROUTE_ID = 'FragmentContainer';
//   int selectedFragment ;
//   int categoryId;
//   int subCategoryId;
//   String tooltitle = "";
//
//   FragmentContainer({
//     Key key,
//     @required this.selectedFragment,
//     this.tooltitle, this.
//     categoryId, this.subCategoryId
//   }) : super(key: key);
//
//   @override
//   State<StatefulWidget> createState() {
//     return new FragmentContainerState();
//   }
// }
//
// class FragmentContainerState extends State<FragmentContainer> {
//   int _selectedDrawerIndex = 0;
//   int counter = 1;
//
//   _getDrawerItemWidget(int pos) {
//     switch (pos) {
//       // case 0:
//       //   return new Categories(
//       //     tooltitle: widget.tooltitle, categoryId: widget.categoryId,
//       //   );
//       // case 1:
//       //   return new SubCategories(
//       //     tooltitle: widget.tooltitle, subCatId: widget.subCategoryId,
//       //   );
//       case 2:
//         return new HomeScreen();
//       case 3:
//         return new ShiftScreen();
//       case 4:
//         return new SalaryScreen();
//
//       default:
//         return new Text("Error");
//     }
//   }
//
//   static openSalaryScreen(BuildContext context, String title) async {
//     await Navigator.push(context, MaterialPageRoute(
//       builder: (context) {
//         return FragmentContainer(
//           selectedFragment: 4,
//           tooltitle: title,
//         );
//       },
//     ));
//   }
//
//   static openHomeScreen(BuildContext context) async {
//     await Navigator.push(context, MaterialPageRoute(
//       builder: (context) {
//         return HomeScreen();
//       },
//     ));
//   }
//
//   static openShiftScreen(BuildContext context) async {
//     await Navigator.push(context, MaterialPageRoute(
//       builder: (context) {
//         return HomeScreen();
//       },
//     ));
//   }
//
//   _onSelectItem(int index) {
//     setState(() => _selectedDrawerIndex = index);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     _selectedDrawerIndex = widget.selectedFragment;
//     var screenWidth = MediaQuery.of(context).size.width;
//     return Scaffold(
//       appBar: AppBar(
//         elevation: 0.0,
//         leading: IconButton(
//           icon: Icon(
//             Icons.arrow_back,
//           ),
//           iconSize: 35,
//           color: appBarColor,
//           splashColor: colorAccent,
//           onPressed: () {
//             Navigator.of(context).pop(true);
//           },
//         ),
//         titleSpacing: 0.0,
//         centerTitle: false,
//         actions: [
//           new Center(
//               child: Padding(
//             padding: EdgeInsets.only(left: 5, right: 5),
//             child: ImageIcon(new AssetImage("assets/icons/search.png"),
//                 color: white_dim, size: 25),
//           )),
//           Center(
//               child: Padding(
//                   padding: EdgeInsets.only(right: 1.0),
//                   child: Stack(
//                     children: <Widget>[
//                       new IconButton(
//                           icon: ImageIcon(
//                               new AssetImage("assets/icons/bell.png"),
//                               color: white_dim,
//                               size: 25),
//                           onPressed: () {
//                             setState(() {
//                               counter += 1;
//                             });
//                           }),
//                       counter != 0
//                           ? new Positioned(
//                               right: 10,
//                               top: 1,
//                               child: new Container(
//                                 padding: EdgeInsets.all(2),
//                                 decoration: new BoxDecoration(
//                                   color: primaryColor,
//                                   borderRadius: BorderRadius.circular(7),
//                                 ),
//                                 constraints: BoxConstraints(
//                                   minWidth: 14.0,
//                                   minHeight: 14.0,
//                                 ),
//                                 child: Text(
//                                   '$counter',
//                                   style: SemiBold16Style.copyWith(
//                                       color: appBarColor, fontSize: 7.0),
//                                   textAlign: TextAlign.center,
//                                 ),
//                               ),
//                             )
//                           : new Container()
//                     ],
//                   )))
//         ],
//         title: SizedBox(
//           width: screenWidth * .6,
//           child: Text(
//             widget.tooltitle,
//             textAlign: TextAlign.start,
//             maxLines: 2,
//             style: SemiBold18Style.copyWith(color: appBarColor),
//           ),
//         ),
//         backgroundColor: colorAccent,
//       ),
//       body: _getDrawerItemWidget(widget.selectedFragment),
//       bottomNavigationBar: Container(
//         color: colorAccent,
//         height: 60.0,
//         child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           crossAxisAlignment: CrossAxisAlignment.center,
//           children: [
//             new GestureDetector(
//                 onTap: () {
//                   openHome(context);
//                 },
//                 child: Padding(
//                   padding: EdgeInsets.only(left: 10.0, right: 10.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       new Container(
//                           width: 30.0,
//                           height: 30.0,
//                           child: Image.asset('assets/icons/home.png')),
//                       Text(
//                         "Home",
//                         style: SemiBold16Style.copyWith(
//                             color: appBarColor, fontSize: 10.0),
//                       ),
//                     ],
//                   ),
//                 )),
//             new GestureDetector(
//                 onTap: () {
//                   openBooking(context, "My Bookings");
//                 },
//                 child: Padding(
//                   padding: EdgeInsets.only(left: 5.0, right: 5.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       new Container(
//                           width: 30.0,
//                           height: 30.0,
//                           child: Image.asset('assets/icons/booking.png')),
//                       Text(
//                         "Bookings",
//                         style: SemiBold16Style.copyWith(
//                             color: appBarColor, fontSize: 10.0),
//                       ),
//                     ],
//                   ),
//                 )),
//             new GestureDetector(
//                 onTap: () {
//                   openHome(context);
//                 },
//                 child: Padding(
//                   padding: EdgeInsets.only(left: 5.0, right: 5.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       new Container(
//                         width: 30.0,
//                         height: 30.0,
//                         child: Stack(
//                           children: <Widget>[
//                             new Container(
//                                 width: 30.0,
//                                 height: 30.0,
//                                 child: Image.asset('assets/icons/tickets.png')),
//                             new Positioned(
//                               right: 0,
//                               top: 0,
//                               child: new Container(
//                                 padding: EdgeInsets.all(2),
//                                 decoration: new BoxDecoration(
//                                   color: Colors.white,
//                                   borderRadius: BorderRadius.circular(7),
//                                 ),
//                                 constraints: BoxConstraints(
//                                   minWidth: 14,
//                                   minHeight: 14,
//                                 ),
//                                 child: Text(
//                                   "2",
//                                   textAlign: TextAlign.center,
//                                   style: SemiBold16Style.copyWith(
//                                       color: colorAccent, fontSize: 7.0),
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                         /* Text(
//                       "11",
//                       style: SemiBold16Style.copyWith(
//                           color: appBarColor),
//                     )*/
//                       ),
//                       Text(
//                         "Tickets",
//                         style: SemiBold16Style.copyWith(
//                             color: appBarColor, fontSize: 10.0),
//                       ),
//                     ],
//                   ),
//                 )),
//             new GestureDetector(
//                 onTap: () {
//                   openWallet(context);
//                 },
//                 child: Padding(
//                   padding: EdgeInsets.only(left: 5.0, right: 5.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       new Container(
//                           width: 30.0,
//                           height: 30.0,
//                           child: Image.asset('assets/icons/wallet.png')),
//                       Text(
//                         "Wallet",
//                         style: SemiBold16Style.copyWith(
//                             color: appBarColor, fontSize: 10.0),
//                       ),
//                     ],
//                   ),
//                 )),
//             new GestureDetector(
//                 onTap: () {
//                   openMoreSetting(context);
//                 },
//                 child: Padding(
//                   padding: EdgeInsets.only(left: 5.0, right: 5.0),
//                   child: Column(
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     children: <Widget>[
//                       new Container(
//                           width: 30.0,
//                           height: 30.0,
//                           child: Image.asset('assets/icons/more.png')),
//                       Text(
//                         "More",
//                         style: SemiBold16Style.copyWith(
//                             color: appBarColor, fontSize: 10.0),
//                       ),
//                     ],
//                   ),
//                 )),
//           ],
//         ),
//       ),
//     );
//   }
// }
