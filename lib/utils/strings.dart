const String loginText = "Login";
const String phoneText = "Phone No.";
const String passwordText = "Password";
const String continueText = "Continue";
const String termsConditionsText = "Agree with our terms & conditions";
const String setupPinText = "Setup Quick Access Pin";
const String mPinText = "Enter M-Pin";
const String confirmMPinText = "Confirm M-Pin";
const String skipNowText = "Skip for now";
const String securePinText = "Enter Secure Pin";
const String myAttendanceText = "My Attendance";
const String mySalaryText = "My Salary";
const String aboutText = "About";
const String settingText = "Setting";
const String profileText = "Profile";
const String homeText = "Home";
const String shiftText = "Shift";
const String salaryText = "Salary";
const String attendanceText = "Attendance";
const String editProfileText = "Edit Profile";
const String nameText = "Name";
const String emailText = "Email";
const String updateText = "Update";
const String changePasswordText = "Change Pin?";
const String startShiftText = "Start Shift";
const String startBreakText = "Start Break";
const String endBreakText = "End Break";
const String EndShiftText = "End Shift";
const String quickAccessText = "Quick access to the log of your attendance";
const String enterPinText = "Insert pin";




