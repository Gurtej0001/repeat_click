import 'package:flutter/material.dart';

// orange color = #FB6816
// Dark Blue = #031344
// Leave = #FF3434
// light gray = #EBEBEB
// text-color = #8A8A8A
const Color orangeColor = Color(0xFFFB6816);
const Color darkBlue = Color(0xFF031344);
const Color leaveColor = Color(0xFFFF3434);
const Color textColor = Color(0xFF8A8A8A);
const Color blackColor = Color(0xFF000000);
const Color whiteColor = Color(0xFFFFFFFF);
const Color greyColor = Color(0xFF8A8A8A);
const Color offWhite = Color(0xFFe3e3e1);