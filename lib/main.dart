import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:repeat_click/API/web_API.dart';
import 'package:repeat_click/Colors/Colors.dart';
import 'package:repeat_click/ui/HomeBaseScreen.dart';
import 'package:repeat_click/ui/LoginScreen.dart';
import 'package:repeat_click/ui/SecurePinScreen.dart';
import 'package:repeat_click/ui/introPage.dart';
import 'package:shared_preferences/shared_preferences.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).requestFocus(new FocusNode());
    return new MaterialApp(
      home: MyHomePage(),
      debugShowCheckedModeBanner: false,
      builder: EasyLoading.init(),
      theme: ThemeData(primarySwatch: MaterialColor(0xFFFB6816, {
        50:  Color(0xFFFB6816),
        100: Color(0xFFFB6816),
        200: Color(0xFFFB6816),
        300: Color(0xFFFB6816),
        400: Color(0xFFFB6816),
        500: Color(0xFFFB6816),
        600: Color(0xFFFB6816),
        700: Color(0xFFFB6816),
        800: Color(0xFFFB6816),
        900: Color(0xFFFB6816),
      })),

    );
  }


}

class MyHomePage extends StatefulWidget {
  @override
  SplashScreenState createState() => SplashScreenState();
}

class SplashScreenState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Timer(
        Duration(seconds: 5),
        () => isIntroPage());
  }
  isIntroPage()async{
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    getIsIntro(WebApiConstaint.kIsIntroScreen).then((value) {
      if (!value) {
        if (prefs.containsKey(WebApiConstaint.kQuickPin)){

          Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => SecurePinScreen()),
          );
        }else{
          Navigator.push(context, MaterialPageRoute(
            builder: (context) {
              return LoginScreen();
            },
          ));
        }


      }else{
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => IntroPage()));
      }
    });
  }
  Future<bool> getIsIntro(String key) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    var value = prefs.getBool(key);
    return value != null ? false : true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        primary: false,
        backgroundColor: orangeColor,
        body: Center(
          child:
          // Text("LOGO", style: TextStyle(fontFamily: "PoppinsRegular",
          //     fontWeight: FontWeight.w800,
          //     color: Colors.white),),
          Image.asset(
            "assets/image/logo.png",
            // width: 150,
            // height: 100,
            fit: BoxFit.fill,
          ),
        ));
  }
}

/*
class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  List<IntroModel> mySLides = new List<IntroModel>();
  int slideIndex = 0;
  PageController controller;

@override
  void initState() {
    
    super.initState();
    mySLides = getSlides();
    controller = new PageController();
    Timer.periodic(Duration(seconds: 4), (Timer timer) {
      if (slideIndex < 2) {
        slideIndex++;
      } else {
        slideIndex = 0;
      }

      controller.animateToPage(
        slideIndex,
        duration: Duration(milliseconds: 500),
        curve: Curves.easeIn,
      );
    });
    //getUserData();
    setState(() {

    });
  }
  
Future<bool> _onWillPop() async {
    return (await showDialog(
          context: context,
          builder: (context) => new AlertDialog(
            title: new Text('Are you sure?'),
            content: new Text('Do you want to exit an App'),
            actions: <Widget>[
              new FlatButton(
                onPressed: () => Navigator.of(context).pop(false),
                child: new Text('No'),
              ),
              new FlatButton(
                onPressed: () => SystemNavigator.pop(),
                child: new Text('Yes'),
              ),
            ],
          ),
        )) ??
        false;
  }
  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    var screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child:PageView(
          controller : controller,
          onPageChanged: (index){
            setState(() {
              slideIndex = index;
            });
          },
        ) ,),
    );
    
  }
  
}
 _buildPageIndicator(bool isCurrentPage) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 2.0),
    height: isCurrentPage ? 10.0 : 6.0,
    width: isCurrentPage ? 10.0 : 6.0,
    decoration: BoxDecoration(
      //color: isCurrentPage ? colorAccent : white_dim,
      borderRadius: BorderRadius.circular(12),
    ),
  );
}
class SlideTile extends StatelessWidget {
  String imagePath, title, desc;
  int index = 0, size = 0;

  SlideTile({this.imagePath, this.title, this.desc, this.size, this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20),
      alignment: Alignment.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          FittedBox(
            child: Image.asset(imagePath),
            fit: BoxFit.fill,
          ),
          SizedBox(
            height: 20,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                for (int i = 0; i < size; i++)
                  i == index
                      ? _buildPageIndicator(true)
                      : _buildPageIndicator(false),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            title,
            textAlign: TextAlign.center,
            //style: Bold20Style.copyWith(color: primaryTextColor),
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            desc,
            textAlign: TextAlign.center,
            //style: Regular14Style.copyWith(color: primaryTextColor),
          )
        ],
      ),
    );
  }
}
*/
