class ShiftModel {
  String time;
  String title;

  ShiftModel({this.time, this.title});

  ShiftModel.fromJson(Map<String, dynamic> json) {
    time = json['time'];
    title = json['title'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['time'] = this.time;
    data['title'] = this.title;
    return data;
  }
}