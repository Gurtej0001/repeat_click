class IntroModel{

  String imageAssetPath;
  String title;
  String desc;

  IntroModel({this.imageAssetPath,this.title,this.desc});

  void setImageAssetPath(String getImageAssetPath){
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle){
    title = getTitle;
  }

  void setDesc(String getDesc){
    desc = getDesc;
  }

  String getImageAssetPath(){
    return imageAssetPath;
  }

  String getTitle(){
    return title;
  }

  String getDesc(){
    return desc;
  }

}

 List<IntroModel> getSlides(){

  List<IntroModel> slides = new List<IntroModel>();
  IntroModel sliderModel = new IntroModel();

  //1
  sliderModel.setDesc("Discover Restaurants offering the best fast food food near you on Foodwa");
  sliderModel.setTitle("Quick Attendence");
  sliderModel.setImageAssetPath("assets/image/group1.png");
  slides.add(sliderModel);

  sliderModel = new IntroModel();

  //2
  sliderModel.setDesc("Our veggie plan is filled with delicious seasonal vegetables, whole grains, beautiful cheeses and vegetarian proteins");
  sliderModel.setTitle("Quick Leave");
  sliderModel.setImageAssetPath("assets/image/group2.png");
  slides.add(sliderModel);

  sliderModel = new IntroModel();

  //3
  sliderModel.setDesc("Food delivery or pickup from local restaurants, Explore restaurants that deliver near you.");
  sliderModel.setTitle("Quick Salery");
  sliderModel.setImageAssetPath("assets/image/group3.png");
  slides.add(sliderModel);

  sliderModel = new IntroModel();

  return slides;
}