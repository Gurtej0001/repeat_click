class AttendanceModel {
  int date;
  String arrive;

  AttendanceModel({this.date, this.arrive});

  AttendanceModel.fromJson(Map<String, dynamic> json) {
    date = json['date'];
    arrive = json['arrive'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = this.date;
    data['arrive'] = this.arrive;
    return data;
  }
}