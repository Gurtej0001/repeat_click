

import 'package:shared_preferences/shared_preferences.dart';

class WebApiConstaint {
//http://192.168.1.22/repeatclickashish/api/v1/staff/
  // Api base url
  static const String BASE_URL = "http://www.microsoftexchange.co.uk/api/v1/staff/";


  static const String API_URL_LOGIN = BASE_URL + "login";
  static const String API_URL_UPDATE_PROFILE = BASE_URL + "updateProfile";
  static const String API_URL_STAFF_SALARY = BASE_URL + "staffSalaryMonthly";
  static const String API_URL_STAFF_ATTENDENCE = BASE_URL + "staffAttendanceMonthly";
  static const String API_URL_GET_SHIFT = BASE_URL + "getShift";
  static const String API_URL_ATTENDENCE = BASE_URL + "staffAttendance";




  // static const String API_URL_GET_OTP = BASE_URL + "getOtp";
  // static const String API_URL_CONFIRM_OTP = BASE_URL + "confirmOtp";
  // static const String API_URL_SET_QUICK_PIN = BASE_URL + "setQuickPin";
  // static const String API_URL_SIGN_IN_WITH_PIN = BASE_URL + "signInWithPin";
  // static const String API_URL_GET_USER_PROFILE = BASE_URL + "getUserProfile";
  // static const String API_URL_UPDATE_PROFILE = BASE_URL1 + "updateUserProfile";
  // static const String API_URL_USER_DASHBOARD = BASE_URL1 + "userDashboard";
  // static const String API_URL_GET_SAVED_ADDRESS = BASE_URL1 + "getAddress";
  // static const String API_URL_ADD_ADDRESS = BASE_URL1 + "addAddress";
  // static const String API_URL_UPDATE_ADDRESS = BASE_URL1 + "updateAddress";
  // static const String API_URL_DELETE_ADDRESS = BASE_URL1 + "deleteAddress?addressId=";
  // static const String API_URL_SUB_CATEGORY = BASE_URL1 + "getSubCategory?parentCatId=";
  // static const String API_URL_GET_PROMOCODE = BASE_URL1 + "getPromoCodes?serviceId=";
  // static const String API_URL_GET_SERVICES = BASE_URL1 + "getServices?categoryId=";
  // static const String API_URL_ADD_SERVICE_RATING = BASE_URL1 + "addServicecRating";
  // static const String API_URL_GET_SERVICE_DETAILS = BASE_URL1 + "getServiceDetails?serviceId=";
  // static const String API_URL_GET_SKILLS = BASE_URL1 + "showcaseSkillCategory?parentCat=6";
  // static const String API_URL_GET_NOTIFICATIONS = BASE_URL1 + "getNotifications";


//SharedPreferences
  static const String kIsIntroScreen = "isIntro";
  static const String kUserData = "userData";
  static const String kAuthKey = "authenticationKey";
  static const String kSessionKey = "sessionKey";
  static const String kQuickPin = "quickPin";


  //Regex
  static final RegExp nameRegExp = RegExp('[a-zA-Z]');
  static final RegExp emailRexExp = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+");

}
