import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'package:path/path.dart';
import 'package:repeat_click/API/loggingInterceptor.dart';
import 'package:repeat_click/API/web_API.dart';
import '../main.dart';


class ApiProvider {
  Dio _dio = new Dio();

  ApiProvider() {
    BaseOptions options = BaseOptions(
        baseUrl: WebApiConstaint.BASE_URL,
        receiveTimeout: 5000,
        connectTimeout: 5000);
    _dio = Dio(options);
    _dio.interceptors.add(LoggingInterceptor());
  }


  Future<Response> requestPostForApi(Map<dynamic, dynamic>  dictParameter , String url , String header, String sessionKey) async {
    _dio.options.contentType = 'application/json';
    _dio.options.headers['authentication'] = header;
    _dio.options.headers['authkey'] = sessionKey;
    Response response = await _dio.post(url , data:dictParameter);
    try {
      print(response);

      return response;
    } catch (error, stacktrace) {
      if (error.response != null)
        return response;
      print("Exception occured: $error stackTrace: $stacktrace");
      EasyLoading.dismiss();
      return null;
      // return UserResponse.withError(_handleError(error));
    }
  }

  Future<Response> requestGetForApi(String url, String header, String sessionKey) async {
    _dio.options.contentType = 'application/json';
    _dio.options.headers['authentication'] = header;
    _dio.options.headers['authkey'] = sessionKey;

    Response response = await _dio.get(url);
    print(header);
    print(sessionKey);

    try {
      print(response);

      return response;
    } catch (error, stacktrace) {

      if (error.response != null)
        return response;
      print("Exception occured: $error stackTrace: $stacktrace");
      EasyLoading.dismiss();
      return null;
      // return UserResponse.withError(_handleError(error));
    }
  }

  Future<Response>uploadImage(dynamic data,String header,String sessionKey,String url) async
  {
    Response response =  await _dio.post(url,data: data,options: Options(contentType: Headers.jsonContentType, headers: {
      "authentication": header,
      "authkey":sessionKey
    })
    );
    try{
      return response;
    }catch (error, stacktrace) {
      if (error.response != null)
        return response;
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
    // if( response.statusCode== 201){
    //   return response;
    // }else{
    //   print('errr');
    //   return null;
    // }
  }


  String _handleError(Error error) {
    String errorDescription = "";
    if (error is DioError) {
      DioError dioError = error as DioError;
      switch (dioError.type) {
        case DioErrorType.CANCEL:
          errorDescription = "Request to API server was cancelled";
          break;
        case DioErrorType.CONNECT_TIMEOUT:
          errorDescription = "Connection timeout with API server";
          break;
        case DioErrorType.DEFAULT:
          errorDescription =
              "Connection to API server failed due to internet connection";
          break;
        case DioErrorType.RECEIVE_TIMEOUT:
          errorDescription = "Receive timeout in connection with API server";
          break;
        case DioErrorType.RESPONSE:
          errorDescription =
              "Received invalid status code: ${dioError.response.statusCode}";
          break;
        case DioErrorType.SEND_TIMEOUT:
          errorDescription = "Send timeout in connection with API server";
          break;
      }
    } else {
      errorDescription = "Unexpected error occured";
    }
    return errorDescription;
  }
}
