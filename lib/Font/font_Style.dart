import 'package:flutter/material.dart';
import 'package:repeat_click/Colors/Colors.dart';

/*
* TextField hint style
*/
final TextStyle hintStyle = TextStyle(
  fontSize: 18.0,
  color: greyColor,
  fontFamily: 'PoppinsRegular',
);

/*
* TextField text style
*/
final TextStyle textFieldTextStyle = TextStyle(
  fontSize: 18.0,
  color: blackColor,
  fontFamily: 'PoppinsRegular',
);

/*
* TextField border normal
*/
final OutlineInputBorder border = OutlineInputBorder(
  borderSide: const BorderSide(color: greyColor, width: 1.0),
  borderRadius: BorderRadius.circular(30.0),
);

/*
* TextField border on focus
*/
final OutlineInputBorder focusedBorder = OutlineInputBorder(
  borderSide: const BorderSide(color: orangeColor, width: 1.0),
  borderRadius: BorderRadius.circular(30.0),
);

/*
* button style
*/
final TextStyle buttonTextStyle = TextStyle(
  fontSize: 16.0,
  color: whiteColor,
  fontFamily: 'PoppinsRegular',
);


/*
* button rounded shape
*/
final RoundedRectangleBorder shape =
    RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0));

final TextStyle titleTextStyle = TextStyle(
  fontSize: 24.0,
  color: orangeColor,
  fontFamily: 'PaytoneOne',

);

final TextStyle orangeRegularTextStyle = TextStyle(
  fontSize: 16.0,
  color: orangeColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle whiteRegularTextStyle = TextStyle(
  fontSize: 18.0,
  color: whiteColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle drawerTitleTextStyle =
    TextStyle(color: textColor, fontSize: 14, fontFamily: 'PoppinsSemiBold');

final TextStyle drawerSubTitleTextStyle =
    TextStyle(color: textColor, fontSize: 12, fontFamily: 'PoppinsRegular');

final TextStyle normal20TextStyle = TextStyle(
  fontSize: 20.0,
  color: textColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle normal18TextStyle = TextStyle(
  fontSize: 18.0,
  color: textColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle normal16TextStyle = TextStyle(
  fontSize: 16.0,
  color: textColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle normal14TextStyle = TextStyle(
  fontSize: 14.0,
  color: textColor,
  fontFamily: 'PoppinsRegular',
);

final TextStyle normal12TextStyle = TextStyle(
  fontSize: 12.0,
  color: textColor,
  fontFamily: 'PoppinsRegular',
);